<?php
$this->assign('title', __('Productos'));
$this->assign('section', __('Productos'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Editar') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Editar') ?></li>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-body">
      <form method="post" enctype="multipart/form-data">
        <div class="form-group has-feedback">
          <label><?= __('Marca / Linea') ?> : <?= h($product->tbl_family['name']) ?></label>
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Código') ?> <span class="text-danger">*</span> </label>
          <input type="text" readonly="true" required="true" maxlength="8" minlength="8" name="code" id="code" value="<?= h($product->code) ?>" class="form-control" placeholder="<?= __('Código') ?>">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Tipo producto') ?> <span class="text-danger">*</span>  </label>
          <input type="text" required="true" maxlength="255" name="type_product" value="<?= h($product->type_product) ?>" class="form-control" placeholder="<?= __('Tipo producto') ?>">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Series') ?></label>
          <textarea name="product_series" maxlength="255" class="form-control" rows="1"><?= h($product->product_series) ?></textarea>
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Modelos') ?> </label>
          <textarea name="models" maxlength="255" class="form-control" rows="1"><?= h($product->models) ?></textarea>
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Presentacion') ?> <span class="text-danger">*</span> </label>
          <input type="text" required="true" maxlength="255" name="presentation" value="<?= h($product->presentation) ?>" class="form-control" placeholder="<?= __('Presentacion') ?>">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Cantidad presentacion') ?></label>
          <input type="text" name="per_master" value="<?= h($product->per_master) ?>" maxlength="10" onKeyPress="javascript:return solo_numeros ( event )" class="form-control" placeholder="<?= __('Cantidad presentacion') ?>">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Weight') ?></label>
          <input type="text" name="weight" value="<?= h($product->weight) ?>" maxlength="255" class="form-control" placeholder="<?= __('Weight') ?>">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Precio sugerido') ?></label>
          <input type="text" name="price_suggested" value="<?= h($product->price_suggested) ?>" maxlength="10" class="form-control" placeholder="<?= __('Precio sugerido') ?>">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Seleccione status') ?> <span class="text-danger">*</span> </label>
          <select class="form-control" name="status" id="status" placeholder="Status">
            <option <?= $product->status == 'NA' ? 'selected' : '' ?> value="NA"><?= __('NA') ?></option>
            <option <?= $product->status == 'N' ? 'selected' : '' ?> value="N"><?= __('Nuevo') ?></option>
            <option <?= $product->status == 'D' ? 'selected' : '' ?> value="D"><?= __('Descuento') ?></option>
            <option <?= $product->status == 'L' ? 'selected' : '' ?> value="L"><?= __('Liquidacion') ?></option>
            <option <?= $product->status == 'PM' ? 'selected' : '' ?> value="PM"><?= __('Precio mejorado') ?></option>
          </select>
        </div>
        <div class="container-fluid">
          <img style="border:1px solid #000;border-radius:10px" width="100%" src="/images/products/<?= $product->img ?>" alt="">
        </div>
        <hr>
        <div class="row">
          <div class="col-xs-4">
            <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-danger">
              <?= __('Cancelar') ?>
            </a>
          </div>
          <div class="col-xs-offset-4 col-xs-4 text-right">
            <button type="submit" class="btn btn-primary family_edit_family"><?= __('Guardar') ?></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>


<!--  CROSS REFERENCES PRODUCTS -->

<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Cross references') ?></small>
  </h1>
</section>

<section class="content">
  <div class="box">
    <div class="box-body">
      <?php if( $cross_references->count() == 0 ): ?>
        <div class="alert alert-info">
          <?= __('No hay productos asociados para mostrar.') ?>
        </div>
      <?php else: ?>
        <table class="table table-condensed dataTables table-striped table-hover">
          <thead>
            <tr>
              <th><?= __('Còdigo') ?></th>
              <th><?= __('Tipo producto') ?></th>
              <th><?= __('Status') ?></th>
              <th style="text-align: center;"><?= __('Fecha creacion') ?></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($cross_references as $references): ?>
              <tr>
                <td>
                  <?= h($references->tbl_product->code) ?>
                </td>
                <td>
                  <?= h($references->tbl_product->type_product) .' - '.  h($references->tbl_product->models) ?>
                </td>
                <td>
                  <?
                  switch ($references->tbl_product->status) {
                    case 'N':
                    echo 'NUEVO';
                    break;
                    case 'D':
                    echo 'DESCUENTO';
                    break;
                    case 'L':
                    echo 'LIQUIDACION';
                    case 'PM':
                    echo 'PRECIO MEJORADO';
                    break;
                    case 'NA':
                    echo 'NO APLICA';
                    break;
                  }
                  ?>
                </td>
                <td align="center">
                  <?= h($references->created->format('m/d/y h:i')) ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      <?php endif; ?>
    </div>
  </div>
</section>
