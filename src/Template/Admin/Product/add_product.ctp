<?php
$this->assign('title', __('Productos'));
$this->assign('section', __('Productos'));
?>

<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Crear') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Crear') ?></li>
  </ol>
</section>
<div id="message" class="container"></div>
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= __('Crear') ?></h3>
    </div>
    <div class="box-body">
      <form method="post" id="form_product" autocomplete="off">
        <div class="form-group has-feedback">
          <label><?= __('Seleccione marca / linea') ?> <span class="text-danger">*</span> </label>
          <select name="id_family" id="id_family" required="true" class="form-control">
            <?php foreach($families as $family): ?>
            <option value="<?= h($family->code) ?>"><?= h($family->code) ?> - <?= h($family->tbl_brand['name']) ?> - <?= h($family->name) ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="form-group has-feedback">
        <label><?= __('Código') ?> <span class="text-danger">*</span> </label>
        <input type="text" required="true" maxlength="8" minlength="8" name="code" id="code" class="form-control" placeholder="<?= __('Código') ?>">
      </div>
      <div class="form-group has-feedback">
        <label><?= __('Tipo producto') ?> <span class="text-danger">*</span> </label>
        <input type="text" required="true" maxlength="255" name="type_product" class="form-control" placeholder="<?= __('Tipo Producto') ?>">
      </div>
      <div class="form-group has-feedback">
        <label><?= __('Series') ?></label>
        <textarea name="product_series" maxlength="255" class="form-control" rows="1"></textarea>
      </div>
      <div class="form-group has-feedback">
        <label><?= __('Modelos') ?></label>
        <textarea name="models" maxlength="255" class="form-control" rows="1"></textarea>
      </div>
      <div class="form-group has-feedback">
        <label><?= __('Presentacion') ?> <span class="text-danger">*</span> </label>
        <input type="text" required="true" maxlength="255" name="presentation" class="form-control" placeholder="<?= __('Presentacion') ?>">
      </div>
      <div class="form-group has-feedback">
        <label><?= __('Cantidad presentacion') ?></label>
        <input type="text" name="per_master" maxlength="10" onKeyPress="javascript:return solo_numeros ( event )" class="form-control" placeholder="<?= __('Cantidad presentacion') ?>">
      </div>
      <div class="form-group has-feedback">
        <label><?= __('Weight') ?></label>
        <input type="text" name="weight" maxlength="255" class="form-control" placeholder="<?= __('Weight') ?>">
      </div>
      <div class="form-group has-feedback">
        <label><?= __('Precio Sugerido') ?> </label>
        <input type="text" name="price_suggested" maxlength="10" class="form-control" placeholder="<?= __('Precio sugerido') ?>">
      </div>
      <div class="form-group has-feedback">
        <label><?= __('Seleccione status') ?> <span class="text-danger">*</span></label>
        <select class="form-control" name="status" id="status" placeholder="Status">
          <option value="NA"><?= __('NA') ?></option>
          <option value="N"><?= __('Nuevo') ?></option>
          <option value="D"><?= __('Descuento') ?></option>
          <option value="L"><?= __('Liquidacion') ?></option>
          <option value="PM"><?= __('Precio mejorado') ?></option>
        </select>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-danger">
            <?= __('Cancelar') ?>
          </a>
        </div>
        <div class="col-xs-offset-4 col-xs-4 text-right">
          <button type="submit" class="btn btn-primary product_add_product"><?= __('Guardar') ?></button>
        </div>
      </div>
    </form>
  </div>
</div>
</section>

<?php $this->append('scripts'); ?>
<script type="text/javascript">
$(document).ready(function () {
  // Ejecuto funcion para agregar codigo de marca apenas el documento este listo
  addCodeFamily()

  //  Si la Marca Cambia que vuelva y agrege el codigo de la marca
  $('#id_family').change( function () {
    addCodeFamily()
    })
  })

  function addCodeFamily() {
    var codeFamily = $('#id_family').val()
    codeFamily = codeFamily.substr(0,4)
    $('#code').val(codeFamily)
  }

  // OnSubmit validacion family
  $('#form_product').submit( function () {
    let validation = validarFamily()
    if (validation) {} else{
      return false
    }
  })
</script>
<?php $this->end(); ?>
