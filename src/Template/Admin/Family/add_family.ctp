<?php
$this->assign('title', __('Lineas'));
$this->assign('section', __('Lineas'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Crear') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Crear') ?></li>
  </ol>
</section>
<div id="message" class="container"></div>
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= __('Crear') ?></h3>
    </div>
    <div class="box-body">
      <form method="post" id="form_family" enctype="multipart/form-data" autocomplete="off">
        <div class="form-group has-feedback">
          <label><?= __('Seleccione Marca') ?> <span class="text-danger">*</span></label>
          <select name="id_brand" id="id_brand" required="true" class="form-control">
            <?php foreach($brands as $brand): ?>
            <option value="<?= h($brand->code) ?>"><?= h($brand->name) ?></option>
          <?php endforeach; ?>
        </select>
      </div>
        <div class="form-group has-feedback">
          <label><?= __('Código') ?> <span class="text-danger">*</span></label>
          <input type="text" maxlength="4"  required="true" name="code" id="code" class="form-control" placeholder="<?= __('Còdigo') ?>">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Nombre completo') ?> <span class="text-danger">*</span></label>
          <input type="text" required="true" name="name" class="form-control" placeholder="<?= __('Nombre completo') ?>">
        </div>
      <div class="form-group has-feedback">
        <label><?= __('Descripcion') ?></label>
        <textarea name="description" class="form-control" rows="4"></textarea>
      </div>
      <div class="form-group has-feedback">
        <label><?= __('Imagen relacionada') ?></label>
        <input type="file"  accept="image/*" name="img" id="img">
      </div>
      <div class="row">
        <div class="col-xs-4">
          <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-danger">
            <?= __('Cancelar') ?>
          </a>
        </div>
        <div class="col-xs-offset-4 col-xs-4 text-right">
          <button type="submit" class="btn btn-primary family_add_family"><?= __('Guardar') ?></button>
        </div>
      </div>
    </form>
  </div>
</div>
</section>

<?php $this->append('scripts'); ?>
<script type="text/javascript">
$(document).ready(function () {
  // Ejecuto funcion para agregar codigo de marca apenas el documento este listo
  addCodeBrand()

  //  Si la Marca Cambia que vuelva y agrege el codigo de la marca
  $('#id_brand').change( function () {
    addCodeBrand()
  })

  // OnSubmit validacion brands
  $('#form_family').submit( function () {
    let validation = validarBrand()
    if (validation) {} else{
      return false
    }
  })
})
</script>
<?php $this->end(); ?>
