<?php
$this->assign('title', __('Paginas'));
$this->assign('section', __('Paginas'));
?>
<section class="content-header">
	<h1>
		<?= $this->fetch('section') ?>
		<small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de paginas') ?></h3>
		</div>
		<div class="box-body">
			<?php if( $sites->count() == 0 ): ?>
				<div class="alert alert-info">
					<?= __('No hay paginas para mostrar.') ?>
				</div>
			<?php else: ?>
				<table class="table table-condensed table-bordered dataTables table-striped table-hover">
					<thead>
						<tr>
							<th><?= __('Nombre') ?></th>
							<th style="text-align: center;"><?= __('Fecha actualizacion') ?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($sites as $site): ?>
							<tr>
								<td>
									<?= h($site->name) ?>
								</td>
								<td align="center">
									<?= h($site->created->format('m/d/y h:i')) ?>
								</td>
								<?php
								if ($site->name == 'Teminos y condiciones') {
									?>
									<td class="text-right">
										<a href="<?= $this->Url->build(['action' => 'view_terms', $site->id]) ?>" class="btn btn-warning btn-sm site_view_site">
											<i class="fa fa-edit"></i>
										</a>
									</td>
									<?php
								}
								else
								{
									?>
									<td class="text-right">
										<a href="<?= $this->Url->build(['action' => 'view_site', $site->id]) ?>" class="btn btn-warning btn-sm site_view_site">
											<i class="fa fa-edit"></i>
										</a>
									</td>
									<?php
								}
								?>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	</div>
</section>
