<?php
use Cake\Routing\Router;
$path = Router::url('/', true);

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<body>
  <div>
    <img src="http://cherryext.miro.beecloud.me/images/cherry-logo.png" width="220px" alt="">
  </div>
  <hr>
  <div>
    <h3>Hola! </h3>
    <p><b><?= $info['nombres'] ?> <?= $info['apellidos'] ?></b> de la empresa <b><?= $info['compania'] ?></b> ha generado una solicitud de usuario para cherry,</p>
    <p>puede revisarlo en cualquier momento en el apartado <b> <a href="<?= $path ?>/admin/setup/edit-user/<?= $info['id'] ?>"> Usuarios </a> <b> </p>
    <p>Muchas Gracias.</p>
  </div>
</body>
