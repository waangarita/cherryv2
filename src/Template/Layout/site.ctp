<!DOCTYPE html>
<html lang="es">
<head>
  <title> cherry </title>
  <meta charset="utf-8">
  <meta name="description" content="cherry Products">
  <meta name="author" content="" />
  <meta name="keywords" content="global, cherry, products, toner, canon, kyosera" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <!--Favicon-->
  <link rel="shortcut icon" href="<?= $this->Url->build('/img/favicon.ico') ?>" type="image/x-icon">
  <link rel="icon" href="<?= $this->Url->build('/img/favicon.ico') ?>" type="image/x-icon">

  <!-- css files -->
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/bootstrap.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/font-awesome.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/owl.carousel.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/owl.theme.default.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/animate.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/swiper.css') ?>" />

  <!-- this is default skin you can replace that with: dark.css, yellow.css, red.css ect -->
  <link id="pagestyle" rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/skin-blue.css') ?>" />

  <!-- Nuevos Estilos cherry -->
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/new_style.css') ?>" />
  <link rel="stylesheet" href="<?= $this->Url->build('/css/jPages.css') ?>">

  <!-- Google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/css/sweetalert.css') ?>">
</head>
<body>

  <!-- start topBar -->
  <div class="topBar">
    <div class="container">
      <ul class="list-inline pull-left hidden-sm hidden-xs">
        <li><span class="text-primary"> <?= __('Tienes alguna pregunta') ?>? </span> <?= __('Llamanos') ?> +1 305 477 2988 </li>
      </ul>

      <ul class="topBarNav pull-right">
        <?php if ($language == 'ES'): ?>
        <li class="linkdown">
          <a href="javascript:void(0);">
            <img src="<?= $this->Url->build('/maqueta/img/flags/flag-spain.jpg') ?>" class="mr-5" alt="">
            <span class="hidden-xs">
              Español
              <i class="fa fa-angle-down ml-5"></i>
            </span>
          </a>
          <ul class="w-100">
            <li><a href="" onclick="language('EN',event)"><img src="<?= $this->Url->build('/maqueta/img/flags/flag-english.jpg') ?>" class="mr-5" alt=""> English </a></li>
          </ul>
        </li>
        <?php else: ?>
          <li class="linkdown">
            <a href="javascript:void(0);">
              <img src="<?= $this->Url->build('/maqueta/img/flags/flag-english.jpg') ?>" class="mr-5" alt="">
              <span class="hidden-xs">
                English
                <i class="fa fa-angle-down ml-5"></i>
              </span>
            </a>
            <ul class="w-100">
              <li>
                <a href="" onclick="language('ES',event)">
                  <img src="<?= $this->Url->build('/maqueta/img/flags/flag-spain.jpg') ?>" class="mr-5" alt=""> Español
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>
        <li class="linkdown">
          <a href="javascript:void(0);">
            <i class="fa fa-user mr-5"></i>
            <span class="hidden-xs">
              <?= __('Mi cuenta') ?>
              <i class="fa fa-angle-down ml-5"></i>
            </span>
          </a>
          <ul class="w-150">
            <li><a href="<?= $this->Url->build(['action' => 'my-account']) ?>"><?= __('Mi cuenta') ?></a></li>
            <li class="divider"></li>
            <li><a href="<?= $this->Url->build(['action' => 'logout']) ?>"><?= __('Cerrar Sesion') ?></a></li>
          </ul>
        </li>
        <?= $this->element('Partials/cart') ?>
        <li class="linkdown">
          <a href="<?= $this->Url->build(['action' => 'logout']) ?>"> <i class="fa fa-sign-out"></i> <?= __('Cerrar Sesion') ?></a>
        </li>
      </ul>
    </div><!-- end container -->
  </div>
  <!-- end topBar -->

  <!-- start navbar -->
  <div class="navbar yamm navbar-default">
    <div class="container">
      <div class="navbar-header">
        <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="navbar-brand">
          <img src="<?= $this->Url->build('/images/cherry-logo.png') ?>" width="200px" alt="logo">
        </a>
      </div>
      <div class="col-md-2"></div>
      <div  id="navbar-collapse-1" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a href="<?= $this->Url->build(['action' => 'index']) ?>"><?= __('Inicio') ?></a></li>

          <li><a href="<?= $this->Url->build(['action' => 'products']) ?>"><?= __('Productos') ?></a></li>

          <li><a href="<?= $this->Url->build(['action' => 'promotion']) ?>"><?= __('Promociones') ?> </a></li>

          <li><a href="<?= $this->Url->build(['action' => 'faq']) ?>"><?= __('FAQ') ?> </a></li>
        </ul>
      </div><!-- end navbar collapse -->
    </div><!-- end container -->
  </div><!-- end navbar -->

  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <ul>
            <li><a href="<?= $this->Url->build(['action' => 'index']) ?>"><?= __('Inicio') ?></a></li>
            <?= $breadcrumbs ?>
          </ul><!-- end breadcrumb -->
        </div><!-- end col -->
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end breadcrumbs -->

  <!-- start section -->
  <section class="section white-backgorund">
    <div class="container">
      <div class="row">
        <!-- start sidebar -->
        <div class="col-sm-3">
          <div class="widget">
            <h6 class="subtitle text-uppercase hidden-xs"><?= __('Buscar') ?></h6>
            <form id="form-search" action="<?= $this->Url->build(['action' => 'products']) ?>">
              <div class="input-group">
                  <input  style="font-family:Arial, FontAwesome" placeholder="&#xF002; <?= __('Buscar') ?>" value="<?= isset($search) ? $search : '' ?>"value="<?= isset($search) ? $search : '' ?>" class="form-control input-sm radius_default" type="text" id="search" name="search">
                  <span class="input-group-addon radius_default">
                    <a href="#" onclick="document.getElementById('form-search').submit()">
                      <i class="fa fa-search" style="color:#878c94"></i>
                    </a>
                  </span>
                  <span class="input-group-addon radius_default">
                    <a href="#" onclick="$('#search').val('');">
                      <i class="fa fa-times" style="color:#878c94"></i>
                    </a>
                  </span>
              </div>
            </form>
          </div><!-- end widget -->
          <!-- NAVBAR-VERTICAL BRANDS WITH FAMILIES -->
          <?= $this->element('Partials/navbar-vertical') ?>
          <!-- END NAVBAR-VERTICAL BRANDS WITH FAMILIES -->
          <div class="widget hidden-xs">
            <h6 class="subtitle text-uppercase"><?= __('Precios Mejorados') ?></h6>
            <hr>
            <ul class="items">
              <?php foreach ($prices_best AS $product): ?>
                <li>
                  <a href="<?= $this->Url->build(['action' => 'detail_product', $product['code'] ]) ?>" class="product-image">
                    <img src="<?= $this->Url->build($product['img']) ?>" alt="<?= $product['code'] ?>">
                  </a>
                  <div class="product-details">
                    <p class="product-name">
                      <a href="<?= $this->Url->build(['action' => 'detail_product', $product['code'] ]) ?>">
                        <b><?= $product['type_product'] ?></b>
                        <br>
                        <?= $product['product_series'] ?>
                      </a>
                    </p>
                    <?php if ($auth['role_id'] <> 3 ) : ?>
                      <span class="price text-primary">$ <?= number_format($product['price_product'],2) ?></span>
                    <?php endif; ?>
                  </div>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
          
          <?php if ( !($auth['role_id'] == 3  || isset($isCart)) ) : ?>
            <div class="widget hidden-xs">
              <h6 class="subtitle text-uppercase"> <?= __('Pedido') ?></h6>
              <hr class="spacer-10">
              <ul data-id="itemsCart_side" class="items">
                <?php $subtotal = 0; ?>
                <?php  if (count($cart) == 0 ): ?>
                  <li><p class="text-center"> <?= __('No hay productos en pedido') ?> </p></li>
                <?php  else: ?>
                  <?php foreach ($cart AS $product): ?>
                    <li data-id="<?= $product['id'] ?>" data-product="<?= $product->tbl_product['code'] ?>" data-user="<?= $auth['id'] ?>">
                      <a href="<?= $this->Url->build(['action' => 'detail_product', $product->tbl_product['code'] ]) ?>" class="product-image">
                        <img src="<?= $this->Url->build($product->tbl_product['img']) ?>" alt="<?= $product->tbl_product['code'] ?>">
                      </a>
                      <div class="product-details">
                        <div class="close-icon">
                          <a href="<?= $this->Url->build(['action' => 'delete_cart', $product['id']]) ?>"
                            onclick="delete_cart('<?= $product['id'] ?>','¿<?= __("Esta seguro que desea eliminar  de la lista") . $product->tbl_product["type_product"] ?>?',event)">
                            <i class="fa fa-close"></i>
                          </a>
                        </div>
                        <p class="product-name">
                          <a href="<?= $this->Url->build(['action' => 'detail_product', $product->tbl_product['code'] ]) ?>"><?= $product->tbl_product['type_product'] ?></a>
                        </p>
                        <strong><?= $product['amount'] ?></strong> x <span class="price text-primary">$<?= number_format($product['price'],2) ?></span>
                        <?php $subtotal = $subtotal + ($product['amount'] * $product['price']); ?>
                      </div><!-- end product-details -->
                    </li><!-- end item -->
                  <?php endforeach; ?>
                <?php  endif; ?>
              </ul>

              <hr class="spacer-10">
              <strong class="text-dark"><?= __('Subtotal Pedido') ?>:<span data-id="subtotalCart" class="pull-right text-primary">$ <?= number_format($subtotal,2); ?></span></strong>
              <hr class="spacer-10">
              <a href="<?= $this->Url->build(['action' => 'cart' ]) ?>" class="btn btn-default semi-circle btn-block btn-md"><i class="fa fa-shopping-basket mr-10"></i><?= __('Ver Pedido') ?></a>
            </div><!-- end widget -->

            <!-- cart list mobile -->
            <div class="widget hidden-sm hidden-md hidden-lg">
              <button type="button" data-toggle="collapse" data-target="#div-collapse-1" class="navbar-toggle cartSide-list hidden-sm hidden-lg"><?= __('Pedido') ?></button>
              <div id="div-collapse-1" class="navbar-collapse collapse">
                <ul data-id="itemsCart_side" class="nav navbar-nav items">
                  <?php $subtotal = 0; ?>
                  <?php  if (count($cart) == 0 ): ?>
                    <li><p class="text-center"> <?= __('No hay productos en pedido') ?> </p></li>
                  <?php  else: ?>
                    <?php foreach ($cart AS $product): ?>
                      <li data-id="<?= $product['id'] ?>" data-product="<?= $product->tbl_product['code'] ?>" data-user="<?= $auth['id'] ?>">
                        <a href="<?= $this->Url->build(['action' => 'detail_product', $product->tbl_product['code'] ]) ?>" class="product-image">
                          <img src="<?= $this->Url->build($product->tbl_product['img']) ?>" alt="<?= $product->tbl_product['code'] ?>">
                        </a>
                        <div class="product-details">
                          <div class="close-icon">
                            <a href="<?= $this->Url->build(['action' => 'delete_cart', $product['id']]) ?>"
                              onclick="delete_cart('<?= $product['id'] ?>','¿<?= __("Esta seguro que desea eliminar  de la lista") . $product->tbl_product["type_product"] ?>?',event)">
                              <i class="fa fa-close"></i>
                            </a>
                          </div>
                          <p class="product-name">
                            <a href="<?= $this->Url->build(['action' => 'detail_product', $product->tbl_product['code'] ]) ?>"><?= $product->tbl_product['type_product'] ?></a>
                          </p>
                          <strong><?= $product['amount'] ?></strong> x <span class="price text-primary">$<?= number_format($product['price'],2) ?></span>
                          <?php $subtotal = $subtotal + ($product['amount'] * $product['price']); ?>
                        </div><!-- end product-details -->
                      </li><!-- end item -->
                    <?php endforeach; ?>
                  <?php  endif; ?>
                </ul>
                <hr class="spacer-10" />
                <strong class="text-dark"><?= __('Subtotal Pedido') ?>:<span data-id="subtotalCart" class="pull-right text-primary">$ <?= number_format($subtotal,2); ?></span></strong>
                <hr class="spacer-10" />
                <a href="<?= $this->Url->build(['action' => 'cart' ]) ?>" class="btn btn-default semi-circle btn-block btn-md"><i class="fa fa-shopping-basket mr-10"></i><?= __('Ver Pedido') ?></a>
              </div><!-- end div-collapse-1 -->

              
            </div><!-- end widget -->
            <!-- end cart list mobile -->

          <?php endif; ?>
        </div><!-- end col -->
        <!-- end sidebar -->

        <?= $this->fetch('content') ?>
      </div><!-- end row -->

      <!-- best prices mobile -->
      <div class="widget hidden-sm hidden-md hidden-lg">
        <h6 class="subtitle text-uppercase"><?= __('Precios Mejorados') ?></h6>
        <hr>
        <ul class="items">
          <?php foreach ($prices_best AS $product): ?>
            <li>
              <a href="<?= $this->Url->build(['action' => 'detail_product', $product['code'] ]) ?>" class="product-image">
                <img src="<?= $this->Url->build($product['img']) ?>" alt="<?= $product['code'] ?>">
              </a>
              <div class="product-details">
                <p class="product-name">
                  <a href="<?= $this->Url->build(['action' => 'detail_product', $product['code'] ]) ?>">
                    <b><?= $product['type_product'] ?></b>
                    <br>
                    <?= $product['product_series'] ?>
                  </a>
                </p>
                <?php if ($auth['role_id'] <> 3 ) : ?>
                  <span class="price text-primary">$ <?= number_format($product['price_product'],2) ?></span>
                <?php endif; ?>
              </div>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
      <!-- end best prices mobile -->

      <!-- Modal Product Quick View -->
    </div><!-- end container -->
  </section>
  <!-- end section -->

  <!-- BANNER INFERIOR Y FOOTER -->
  <?= $this->element('Partials/footer') ?>
  <!-- END BANNER INFERIOR Y FOOTER -->


  <!-- LENGUAGE -->
  <script type="text/javascript">
    const LANGUAGE = '<?= $language  ?>'
    const API = '<?= $this->Url->build("/api/") ?>'
    const URL_IMAGES = '<?= $this->Url->build('/images/') ?>'
    const ROOT = '<?= $this->Url->build('/') ?>'
  </script>
  <!-- END LENGUAGE -->


  <!-- JavaScript Files -->
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/jquery-3.1.1.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/bootstrap.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/owl.carousel.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/jquery.downCount.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/nouislider.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/jquery.sticky.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/pace.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/star-rating.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/wow.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/swiper.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/main.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/js/translate.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/js/jPages.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/js/sweetalert.min.js') ?>"></script>
<script type="text/javascript" src="<?= $this->Url->build('/js/general.js') ?>"></script>
  <script>

  $(document).ready(function () {

    /* when document is ready */
    getCartByUser (<?= $auth['id'] ?>)

  })
  </script>
  <?= $this->fetch('scripts') ?>
</body>
</html>
