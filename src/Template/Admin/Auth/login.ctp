<?php
$this->assign('title', __('Iniciar Sesion'));
?>
<div class="login-logo">
	<a href="<?= $this->Url->build('/admin/dashboard') ?>">
		<img src="<?= $this->Url->build('/images/cherry-logo.png') ?>" alt="" width="300px">
	</a>
</div>
<div class="login-box-body">
	<p class="login-box-msg"><?= __('Inicia Sesion') ?></p>

	<form method="post">
		<div class="form-group has-feedback">
			<input type="email" name="email" class="form-control" placeholder="<?= __('Email') ?>">
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<input type="password" name="password" class="form-control" placeholder="<?= __('Password') ?>">
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<button type="submit" class="btn btn-primary btn-block btn-flat"><?= __('Entrar') ?></button>
			</div>
		</div>
	</form>

</div>
