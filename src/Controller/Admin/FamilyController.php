<?php

namespace App\Controller\Admin;

class FamilyController extends AdminController {

    public function initialize()
    {
        parent::initialize();
        // load models
        $this->loadModel('TblBrand');
        $this->loadModel('TblFamily');
    }

    /*
    * Family management
    */

    public function index () {
        $family = $this->TblFamily->find()
                                    ->contain(['TblBrand' => function ($q) {
                                            return $q->select(['code','name']);
                                        }]);
        $this->set('families', $family);
    }

    public function addFamily () {
        $brands = $this->TblBrand->find()->select(['code','name']);
        if($this->request->is('post'))
        {
            $exists = $this->TblFamily->verifyFamily($this->request->data('code'));

            if($exists) {
                $this->Flash->warning('Ya existe una familia ¿ con este codigo , no es posible crearla');
            } else {
                $loguser = $this->Auth->user(); //logged user
                $family = $this->TblFamily->newEntity($this->request->data());
                $family->code = $this->request->data('code');
                $family->name = $this->request->data('name');
                $family->id_brand = $this->request->data('id_brand');
                $family->description = $this->request->data('description');

                if(isset($this->request->data['img'])) {
                    $this->log($this->request->data(), 'debug');
                    $file_path = 'images/products/' . $this->request->data('code') . '_default.'.'jpg';
                    $family->img = $file_path;

                    move_uploaded_file($this->request->data['img']['tmp_name'], WWW_ROOT.$file_path);
                }

                if($this->TblFamily->save($family))
                {
                    $successMsg = sprintf("Linea '%s' creada.", $family->name);
                    $this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
                    $this->log($family, 'debug');
                    $this->Flash->success('La linea ha sido creada correctamente.');
                    return $this->redirect(['action' => 'index']);
                }
                else
                {
                    $successMsg = sprintf("Ha ocurrido un error creando la '%s'.", $family->name);
                    $this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
                    $family->errors();
                    $this->Flash->error('La linea no pudo ser creada. Por favor, intente nuevamente.');
                }
            }

        }

        $this->set(compact('brands'));
        $this->set(compact('family'));
    }

    public function editFamily ($id) {
        $family = $this->TblFamily->get($id, ['contain' => 'TblBrand']);
        $this->set(compact('family'));

        if ( $this->request->is('post') ) {
            $loguser = $this->Auth->user(); //logged user
            $delete_img = false;

            $family = $this->TblFamily->patchEntity($family, $this->request->data() );


            if (!empty($this->request->data('delete_img'))) {
                $delete_img = true;
            }

            if (!empty($this->request->data['file_img']['name'])) {
                $delete_img = false;
                $file_path = '/images/products/' . $this->request->data('code') . '_default.'.'jpg';
                $family->img = $file_path;
                move_uploaded_file($this->request->data['file_img']['tmp_name'], WWW_ROOT.$file_path);
            } else {
                $family->img = '';
            }

            if ($delete_img == true) {
                // Delete image
                unlink(WWW_ROOT.$this->request->data['delete_img']);
            }

            if( $this->TblFamily->save($family) ) {
                $successMsg = sprintf("Linea '%s' editado.", $family->name);
                $this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
                $this->log($family ,'debug');
                $this->Flash->success($successMsg);
            } else {
                $failMsg = 'Ha ocurrido un error editado la linea. ';
                $this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
                $this->log($family->errors(), "error");
                $this->Flash->error($failMsg);
            }
            return $this->redirect(['action' => 'index']);
        }
    }

    public function deleteFamily ($id) {
        $family = $this->TblFamily->get($id);
        $familyName = $family->name;
        $loguser = $this->Auth->user(); //logged user
        try {
            if( $this->TblFamily->delete($family) ) {
                $successMsg = sprintf("Linea eliminada. [%s]", $familyName);
                $this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
                $this->Flash->success($successMsg);
                return $this->redirect(['action' => 'index']);
            }
        } catch (\Exception $e) {
            $failMsg = 'Error al eliminar la linea';
            $this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
            $this->Flash->error($failMsg);
            return $this->redirect(['action' => 'index']);
        }
    }
}

?>
