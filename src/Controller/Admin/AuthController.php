<?php

namespace App\Controller\Admin;

use Cake\Auth\DefaultPasswordHasher;
use App\Model\Entity\TblAdmin;

class AuthController extends AdminController
{

	public function initialize()
	{
		parent::initialize();

		$this->viewBuilder()
			->layout('auth');

		// Allow actions
		$allowed = ['registro', 'forgot'];
		$this->Auth->allow($allowed);

		// load models
		$this->loadModel('AdmUser');
		$this->loadModel('TblLogLogin');
	}

	public function login()
	{
		if( $this->request->is('post') ) {
			$user = $this->Auth->identify();
			if( $user ) {
				if($user['status'] == 'ACTIVE'){
					$loguser = $this->logLogin($user['id']);
					$this->Auth->setUser($user);
					return $this->redirect($this->Auth->redirectUrl());
				}
				else
					$this->Flash->error(__('Usuario inactivo'));
			}
			$this->Flash->error(__('Usuario o password incorrectos, intente nuevamente!'));
		}
	}


	public function logout()
	{
		return $this->redirect($this->Auth->logout());
	}

	public function logLogin ($id_user) {
		$log = $this->TblLogLogin->newEntity();
		$log->id_user = $id_user;
		if($this->TblLogLogin->save($log)) {
			return true;
		}else{
			return false;
		}
	}

	/*
	public function registro()
	{
		if( $this->request->is('post') ) {
			$admin = $this->AdmUser->newEntity($this->request->data);
			$admin->status = 'ACTIVE';
			$admin->ceated = date('Y-m-d h:d:s');

			if( $this->AdmUser->save($admin) ) {
				return $this->redirect(['action' => 'login']);
			}
		}
	}
	*/

}
