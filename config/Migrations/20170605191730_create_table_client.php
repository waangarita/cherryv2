<?php

use Phinx\Migration\AbstractMigration;

class CreateTableClient extends AbstractMigration
{
  /**
  * Change Method.
  *
  * Write your reversible migrations using this method.
  *
  * More information on writing migrations is available here:
  * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
  *
  * The following commands can be used in this method and Phinx will
  * automatically reverse them when rolling back:
  *
  *    createTable
  *    renameTable
  *    addColumn
  *    renameColumn
  *    addIndex
  *    addForeignKey
  *
  * Remember to call "create()" or "update()" and NOT "save()" when working
  * with the Table class.
  */
  public function change()
  {
    $tableListPrice = $this->table('tbl_type_list_price');
    $tableListPrice ->  addColumn('name', 'string')
                    ->  addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP', 'null' => 'true' ))
                    ->  addColumn('modified', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
                    ->  create();

    $table = $this->table('tbl_client', ['id' => false, 'primary_key' => 'code']);
    $table  ->  addColumn('code', 'string')
            ->  addColumn('name', 'string')
            ->  addColumn('id_type_list_price', 'integer')
            ->  addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP', 'null' => 'true' ))
            ->  addColumn('modified', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
            ->  create();

    $refTable = $this->table('tbl_client');
    $refTable ->  addForeignKey('id_type_list_price', 'tbl_type_list_price', 'id', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
              ->  update();
  }
}
