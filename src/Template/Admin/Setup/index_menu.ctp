<?php
	$this->assign('title', __('Menu'));
	$this->assign('section', __('Menu'));
?>
<section class="content-header">
	<h1>
        <?= $this->fetch('section') ?>
        <small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
        <li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
        <div class="box-header with-border">
			<a href="<?= $this->Url->build(['action' => 'add_menu']) ?>" class="btn btn-default btn-sm setup_add_menu" title="Agregar" style="float: right;">
				<i class="fa fa-plus"></i>
			</a>
        </div>
        <div class="box-body">
    		<?php if(!empty($menuItems)){ ?>
				<div id="tree" class="col-xs-9"></div>
				<div class="col-xs-3">
					<label><?= __('Acciones')?></label>
					<div class="actions" style="display: none;">
						<a href="#" id="edit-button" class="btn btn-warning btn-sm setup_edit_menu"
						   title="Editar">
							<i class="fa fa-edit"></i>
						</a>
						<a href="#" id="delete-button" class="btn btn-danger btn-sm setup_delete_menu" data-confirm="<?= __('¿Esta seguro que desea eliminar este item?') ?>">
							<i class="fa fa-trash-o"></i>
						</a>
					</div>
				</div>
			<?php }else{ ?>
				<div class="alert alert-info">
                    <?= __('No hay secciones de menu para mostrar.') ?>
                </div>
			<?php } ?>
        </div>
	</div>
</section>

<?php $this->append('scripts'); ?>
<script type="text/javascript">
console.log(<?= json_encode($menuItems) ?>);
$('#tree').treeview({data: <?= json_encode($menuItems) ?> });
$('#tree').treeview('expandAll', { silent: true });

$('#tree').on('nodeSelected', function(event, data) {
  // Your logic goes here
  console.log(data.href);
  $('.actions').show();
  $('#edit-button').attr('href', "<?= $this->Url->build(['action' => 'edit_menu']) ?>/" + data.href);
  $('#delete-button').attr('href', "<?= $this->Url->build(['action' => 'delete_menu']) ?>/" + data.href);
});

</script>
<?php $this->end(); ?>