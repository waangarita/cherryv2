<?php
$this->assign('title', __('Roles'));
$this->assign('section', __('Roles'));
?>
<section class="content-header">
    <h1>
        <?= $this->fetch('section') ?>
        <small><?= __('Editar') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?= $this->Url->build(['action' => 'indexRole']) ?>">
                <?= $this->fetch('section') ?>
            </a>
        </li>
        <li class="active"><?= __('Editar') ?></li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-body">
            <form method="post" enctype="multipart/form-data">
                <div class="form-group has-feedback">
                    <label><?= __('Nombre') ?></label>
                    <input type="text" name="name" class="form-control" placeholder="<?= __('Nombre') ?>" value="<?= $role->name ?>" required>
                    <span class="form-control-feedback"></span>
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-4">
                        <a href="<?= $this->Url->build(['action' => 'indexRole']) ?>" class="btn btn-danger">
                            <?= __('Cancelar') ?>
                        </a>
                    </div>
                    <div class="col-xs-offset-4 col-xs-4 text-right">
                        <button type="submit" class="btn btn-primary setup_edit_role"><?= __('Guardar') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
