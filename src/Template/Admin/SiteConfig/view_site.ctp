<?php
$this->assign('title', __('Paginas'));
$this->assign('section', __('Paginas'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Editar') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Editar') ?></li>
  </ol>
</section>
<form method="post" enctype="multipart/form-data">
  <input type="hidden" id="img_delete" name="img_delete" value="">
  <section class="content">
    <div class="box box-primary">
      <div class="box-body">
        <div class="form-group has-feedback">
          <label>Nombre</label>
          <input type="text" readonly name="name" value="<?= $site->name ?>" class="form-control" placeholder="Nombre completo">
        </div>
        <hr class="separator">
      </div>
      <?php
      if ($slides->count() > 0 ) :
        ?>
        <div class="box-body">
          <div>
            <h2> Slide home</h2>
          </div>
          <?php foreach($slides AS $slide) : ?>
            <div class="box-body template">
              <div class="row">
                <button style="float:right;margin:10px;" type="botton" class="btn btn-danger removeButton"><i class="fa fa-trash"></i></button>
              </div>
              <div class="form-group has-feedback">
                <label>URL <span class="text-danger">*</span></label>
                <input type="text" name="url[]" value="<?= $slide->cta ?>" class="form-control" placeholder="URL">
                <span class="form-control-feedback"></span>
              </div>
              <div class="row">
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label>Slider Desktop. Tamaño sugerido (1380x755)</label>
                    <input type="hidden" class="old" name="hidden[]" value="<?= $slide->img_desktop ?>">
                    <input type="file" name="slider[]" class="form-control slider" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg"  />
                    <br>
                    <div class="imagen" align="center"><img class="blah" src="<?= $slide->img_desktop ?>" alt="your image" style='width:50%;'/></div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group">
                    <label>Slider Mobile. Tamaño sugerido (320x300)</label>
                    <input type="hidden" class="old_mobile" name="hidden_mobile[]" value="<?= $slide->img_mobile ?>">
                    <input type="file" name="slider_mobile[]" class="form-control slider_mobile" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg" />
                    <br>
                    <div class="imagen_mobile" align="center"><img  class="blah_mobile" src="<?= $slide->img_mobile ?>" alt="your image" style='width:50%;'/></div>
                  </div>
                </div>
              </div>
              <hr class="separator">
            </div>
          <?php endforeach; ?>
          <div id="template"></div>
          <div class="col-xs-12">
            <button style="float:right;" onclick="addSlide()" type="button" class="btn btn-info promotion_add_slide"><i class="fa fa-plus"></i> Agregar</button>
          </div>
        </div>
        <?php
      else:
        ?>
        <div class="box-body">
          <div>
            <h2> Slide home</h2>
          </div>
          <div class="form-group has-feedback">
            <label>URL <span class="text-danger">*</span></label>
            <input type="text" name="url[]" class="form-control" placeholder="URL">
            <span class="form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label>Slider Desktop. Tamaño sugerido (1380x755)</label>
                <input type="hidden" class="old" name="hidden[]" value="">
                <input type="file" name="slider[]" class="form-control slider" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg" required />
                <br>
                <div class="imagen" align="center"><img hidden class="blah" src="#" alt="your image" style='width:50%;'/></div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label>Slider Mobile. Tamaño sugerido (320x300)</label>
                <input type="hidden" class="old_mobile" name="hidden_mobile[]" value="">
                <input type="file" name="slider_mobile[]" class="form-control slider_mobile" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg" required />
                <br>
                <div class="imagen_mobile" align="center"><img hidden class="blah_mobile" src="#" alt="your image" style='width:50%;'/></div>
              </div>
            </div>
          </div>
          <hr class="separator">
          <div id="template"></div>
          <div class="col-xs-12">
            <button style="float:right;" onclick="addSlide()" type="button" class="btn btn-info promotion_add_slide"><i class="fa fa-plus"></i> Agregar</button>
          </div>
        </div>
        <?php
      endif;
      ?>
    </div>

    <div class="box box-primary">
      <div class="box-body">
        <div class="col-xs-12">
          <h2> Banner central</h2>
        </div>
        <div class="col-xs-12 col-sm-6">
          <hr class="separator">
          <div class="form-group has-feedback">
            <label>URL <span class="text-danger">*</span></label>
            <input type="text" name="url1" value="<?= $site->cta1 ?>" class="form-control" placeholder="URL">
            <span class="form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label>Slider Desktop. Tamaño sugerido (1380x755)</label>
                <input type="hidden" class="old" name="hidden1" value="<?= $site->slide1 ?>">
                <input type="file" name="slider1" class="form-control slider" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg"  />
                <br>
                <div class="imagen" align="center"><img class="blah" src="<?= $site->slide1 ?>" alt="" style='width:50%;'/></div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label>Slider Mobile. Tamaño sugerido (320x300)</label>
                <input type="hidden" class="old_mobile" name="hidden_mobile1" value="<?= $site->slide_mobile1 ?>">
                <input type="file" name="slider_mobile1" class="form-control slider_mobile" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg"  />
                <br>
                <div class="imagen_mobile" align="center"><img  class="blah_mobile" src="<?= $site->slide_mobile1 ?>" alt="" style='width:50%;'/></div>
              </div>
            </div>
          </div>
          <hr class="separator">
        </div>

        <div class="col-xs-12 col-sm-6">
          <hr class="separator">
          <div class="form-group has-feedback">
            <label>URL <span class="text-danger">*</span></label>
            <input type="text" name="url2" value="<?= $site->cta2 ?>" class="form-control" placeholder="URL">
            <span class="form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label>Slider Desktop. Tamaño sugerido (1380x755)</label>
                <input type="hidden" class="old" name="hidden2" value="<?= $site->slide2 ?>">
                <input type="file" name="slider2" class="form-control slider" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg"  />
                <br>
                <div class="imagen" align="center"><img  class="blah" src="<?= $site->slide2 ?>" alt="" style='width:50%;'/></div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label>Slider Mobile. Tamaño sugerido (320x300)</label>
                <input type="hidden" class="old_mobile" name="hidden_mobile2" value="<?= $site->slide_mobile2 ?>">
                <input type="file" name="slider_mobile2" class="form-control slider_mobile" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg"  />
                <br>
                <div class="imagen_mobile" align="center"><img  class="blah_mobile" src="<?= $site->slide_mobile2 ?>" alt="" style='width:50%;'/></div>
              </div>
            </div>
          </div>
          <hr class="separator">
        </div>

        <div class="col-xs-12 col-sm-6">
          <hr class="separator">
          <div class="form-group has-feedback">
            <label>URL <span class="text-danger">*</span></label>
            <input type="text" name="url3" value="<?= $site->cta3 ?>" class="form-control" placeholder="URL">
            <span class="form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label>Slider Desktop. Tamaño sugerido (1380x755)</label>
                <input type="hidden" class="old" name="hidden3" value="<?= $site->slide3 ?>">
                <input type="file" name="slider3" class="form-control slider" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg"  />
                <br>
                <div class="imagen" align="center"><img class="blah" src="<?= $site->slide3 ?>" alt="" style='width:50%;'/></div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group">
                <label>Slider Mobile. Tamaño sugerido (320x300)</label>
                <input type="hidden" class="old_mobile" name="hidden_mobile3" value="<?= $site->slide_mobile3 ?>">
                <input type="file" name="slider_mobile3" class="form-control slider_mobile" title="Por favor seleccione un archivo" value="<?= $site->slide_mobile3 ?>" accept=".png,.jpg,.jpeg"  />
                <br>
                <div class="imagen_mobile" align="center"><img  class="blah_mobile" src="<?= $site->slide_mobile3 ?>" alt="" style='width:50%;'/></div>
              </div>
            </div>
          </div>
          <hr class="separator">
        </div>
      </div>
    </div>

    <div class="box box-primary">
      <div class="box-body">
        <div>
          <h2> Banner footer </h2>
        </div>
        <div class="form-group has-feedback">
          <label>URL <span class="text-danger">*</span></label>
          <input type="text" name="url_banner" value="<?= $site->cta_banner ?>" class="form-control" placeholder="URL">
          <span class="form-control-feedback"></span>
        </div>
        <div class="row">
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label>Banner Desktop. Tamaño sugerido (1380x755)</label>
              <input type="hidden" class="old" name="banner_hidden" value="<?= $site->banner ?>">
              <input type="file" name="banner" class="form-control slider" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg"  />
              <br>
              <div class="imagen" align="center"><img  class="blah" src="<?= $site->banner ?>" alt="" style='width:50%;'/></div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6">
            <div class="form-group">
              <label>Banner Mobile. Tamaño sugerido (320x300)</label>
              <input type="hidden" class="old_mobile" name="banner_hidden_mobile" value="<?= $site->banner_mobile ?>">
              <input type="file" name="banner_mobile" class="form-control slider_mobile" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg"  />
              <br>
              <div class="imagen_mobile" align="center"><img  class="blah_mobile" src="<?= $site->banner_mobile ?>" alt="" style='width:50%;'/></div>
            </div>
          </div>
        </div>
        <hr class="separator">
      </div>
    </div>

  <div class="row">
    <div class="col-xs-4">
      <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-danger">
        <?= __('Cancelar') ?>
      </a>
    </div>
    <div class="col-xs-offset-4 col-xs-4 text-right">
      <button type="submit" class="btn btn-primary promotion_edit_promotion"><?= __('Guardar') ?></button>
    </div>
  </div>
</section>

</form>

<?php $this->append('scripts'); ?>
<script type="text/javascript">

function addSlide () {

  var content = `
  <div  class="box-body template">
  <div class="form-group has-feedback">
  <button style="float:right;" onclick="$(this).parents('.template').remove()" class="btn btn-danger"><i class="fa fa-trash"></i></button>
  </div>
  <div class="form-group has-feedback">
  <label>URL <span class="text-danger">*</span></label>
  <input type="text" name="url[]" class="form-control" placeholder="URL">
  <span class="form-control-feedback"></span>
  </div>
  <div class="row">
  <div class="col-xs-6">
  <div class="form-group">
  <label>Slider Desktop. Tamaño sugerido (1380x755)</label>
  <input type="hidden" class="old" name="hidden[]" value="">
  <input type="file" name="slider[]" class="form-control slider" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg" required />
  <br>
  <div class="imagen" align="center"><img hidden class="blah" src="#" alt="your image" style='width:50%;'/></div>
  </div>
  </div>
  <div class="col-xs-6">
  <div class="form-group">
  <label>Slider Mobile. Tamaño sugerido (320x300)</label>
  <input type="hidden" class="old_mobile" name="hidden_mobile[]" value="">
  <input type="file" name="slider_mobile[]" class="form-control slider_mobile" title="Por favor seleccione un archivo" value="" accept=".png,.jpg,.jpeg" required />
  <br>
  <div class="imagen_mobile" align="center"><img hidden class="blah_mobile" src="#" alt="your image" style='width:50%;'/></div>
  </div>
  </div>
  </div>
  <hr class="separator">
  </div>`

  $('#template').append(content)
}
</script>

<script>
var deleted = new Array();
var deleted_mobile = new Array();
function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      //$('#blah').attr('src', e.target.result);
      $(input).siblings('.imagen').find('img').attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
  }
  else {
    $(input).siblings('.imagen').find('img').fadeOut(0);
  }
}

function readURL_mobile(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
      //$('#blah').attr('src', e.target.result);
      $(input).siblings('.imagen_mobile').find('img').attr('src', e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
  }
  else {
    $(input).siblings('.imagen_mobile').find('img').fadeOut(0);
  }
}

$('form').on('change', '.slider', function(){
  $(this).siblings('.imagen').find('img').fadeIn(0);
  deleted[deleted.length] = $(this).siblings('.old').val();
  $('#img_delete').val(deleted);
  $(this).siblings('.old').val("");
  readURL(this);
});

$('form').on('change', '.slider_mobile', function(){
  $(this).siblings('.imagen_mobile').find('img').fadeIn(0);
  deleted[deleted.length] = $(this).siblings('.old_mobile').val();
  $('#img_delete').val(deleted);
  $(this).siblings('.old_mobile').val("");
  readURL_mobile(this);
});

// Add button click handler
$('.addButton').on('click', function() {
  if($('.slider').size() < 6){
    var $template = $('#optionTemplate'),
    $inserttemplate = $('#template'),
    $clone    = $template
    .clone()
    .removeClass('hide')
    .removeAttr('id')
    .insertBefore($inserttemplate)
  }
});

// Remove button click handler
$('form').on('click', '.removeButton', function () {
  var $row = $(this).parents('.template')
  deleted[deleted.length] = $row.find('.old').val()
  $('#img_delete').val(deleted)
  deleted[deleted.length] = $row.find('.old_mobile').val()
  $('#img_delete').val(deleted)
  // Remove element containing the option
  $row.remove()
});

</script>
<?php $this->end(); ?>
