<aside class="main-sidebar">

	<section class="sidebar">

		<div class="user-panel">
			<div class="pull-left info-bee" style="color: white;">
				<p><?= $auth['first_name'] .' '.$auth['last_name']  ?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> <?= __('Online') ?></a>
			</div>
		</div>

		<ul class="sidebar-menu">
			<li class="header"><?= __('NAVEGACION') ?></li>
			<?php foreach ($authMenuItems as $menuItem) { ?>
				<?php if(empty($menuItem->Childs)){ ?>
					<li <?= $menuItem->enabledForUser($auth) ?>>
						<a href="<?= $this->Url->build(['controller' => @$menuItem->Section->controller, 'action' => @$menuItem->Section->action]) ?>" class="<?= getSectionClass($menuItem->Section); ?>">
							<i class="fa <?= @$menuItem->icon ?>"></i>
							<span><?= @$menuItem->display_name ?></span>
						</a>
					</li>
				<?php }else{ ?>
					<li class="treeview" <?= @$menuItem->enabledForUser($auth) ?>>
						<a href="#">
							<i class="fa <?= @$menuItem->icon ?>"></i>
							<span><?= @$menuItem->display_name ?></span>
							<i class="fa fa-angle-left pull-right"></i>
						</a>
						<ul class="treeview-menu">
							<?php foreach ($menuItem->Childs as $child) { ?>
							<li <?= @$child->enabledForUser($auth) ?>>
								<a href="<?= $this->Url->build(['controller' => @$child->Section->controller, 'action' => @$child->Section->action]) ?>" class="<?= getSectionClass($child->Section); ?>">
									<i class="fa <?= @$child->icon ?>"></i>
									<span><?= @$child->display_name ?></span>
								</a>
							</li>
							<?php } ?>
						</ul>
					</li>
				<?php } ?>
			<?php } ?>
			<li class="header"><?= __('ACCIONES') ?></li>

			<li>
				<a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'logout']) ?>">
					<i class="fa fa-power-off text-red"></i>
					<span><?= __('Salir') ?></span>
				</a>
			</li>
		</ul>
	</section>
</aside>
