<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>cherry <?= $this->fetch('title') ?></title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="shortcut icon" type="image/png" href="<?= $this->Url->build('favicon.ico') ?>"/>
		<link rel="stylesheet" href="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/bootstrap/css/bootstrap.min.css') ?>">
		<link rel="stylesheet" href="<?= $this->Url->build('/themes/admin/vendor/fontawesome/css/font-awesome.min.css') ?>">
		<link rel="stylesheet" href="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/dist/css/AdminLTE.min.css') ?>">
		<link rel="stylesheet" href="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/dist/css/skins/_all-skins.min.css') ?>">
		<link rel="stylesheet" href="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/plugins/iCheck/all.css') ?>">
		<link rel="stylesheet" href="<?= $this->Url->build('/themes/admin/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') ?>">
		<link rel="stylesheet" href="<?= $this->Url->build('/themes/admin/vendor/bootstrap-treeview/dist/bootstrap-treeview.min.css') ?>">
		<link rel="stylesheet" href="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/plugins/select2/select2.min.css') ?>">

		<!-- custom styles -->
		<link rel="stylesheet" href="<?= $this->Url->build('../css/main.css') ?>">

		<!-- DataTables -->
		<link rel="stylesheet" href="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.css') ?>">

		<style type="text/css">
			.select2-selection__choice{
				color: black !important;
			}
			.separator {
				border-bottom: 1px solid #3C8DBC;
			}
		</style>
	</head>
	<body class="hold-transition skin-black sidebar-mini">

		<div class="wrapper">
			<?= $this->element('Admin/Partials/privileges_manager') ?>

			<?= $this->element('Admin/Partials/header') ?>

			<?= $this->element('Admin/Partials/sidebar') ?>

			<div class="content-wrapper">

				<?= $this->Flash->render() ?>

				<?= $this->fetch('content') ?>

			</div>

			<?= $this->element('Admin/Partials/footer') ?>

		</div>

		<script src="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/plugins/jQueryUI/jquery-ui.min.js') ?>"></script>
		<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
		<script>
			$.widget.bridge('uibutton', $.ui.button);
		</script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/bootstrap/js/bootstrap.min.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/plugins/iCheck/icheck.min.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/plugins/daterangepicker/moment.min.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/plugins/fastclick/fastclick.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/dist/js/app.min.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/tinymce/tinymce.min.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/bootbox.js/bootbox.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/bootstrap-treeview/dist/bootstrap-treeview.min.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/plugins/select2/select2.min.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/plugins/datatables/jquery.dataTables.js') ?>"></script>
		<script src="<?= $this->Url->build('/themes/admin/vendor/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js') ?>"></script>
		<script src="<?= $this->Url->build('/js/general.js') ?>"></script>

		<script type="text/javascript">
		$(document).ready(function () {
			$(".select2").select2();
			$('.dataTables').DataTable();
		});
		</script>
		<?= $this->fetch('scripts') ?>
	</body>
</html>
