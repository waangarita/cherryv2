<?php
	$this->assign('title', __('Secciones'));
	$this->assign('section', __('Secciones'));
?>
<section class="content-header">
	<h1>
        <?= $this->fetch('section') ?>
        <small><?= ('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
        <li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
        <div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de Secciones') ?></h3>
			<a href="<?= $this->Url->build(['action' => 'add_section']) ?>" class="btn btn-default btn-sm setup_add_section" title="Agregar" style="float: right;">
				<i class="fa fa-plus"></i>
			</a>
        </div>
        <div class="box-body">
			<?php if( $sections->count() == 0 ): ?>
			<div class="alert alert-info">
				<?= __('No hay secciones para mostrar.') ?>
			</div>
			<?php else: ?>
			<table class="table table-condensed table-striped table-hover">
				<tr>
					<th><?= __('Seccion') ?></th>
					<th>&nbsp;</th>
				</tr>
				<?php foreach($sections as $section): ?>
				<tr>
					<td>
						<?= h($section->name) ?>
					</td>
					<td class="text-right">
						<a href="<?= $this->Url->build(['action' => 'edit_section', $section->id]) ?>" class="btn btn-warning btn-sm setup_edit_section">
							<i class="fa fa-edit"></i>
						</a>
						<a href="<?= $this->Url->build(['action' => 'delete_section', $section->id]) ?>" class="btn btn-danger btn-sm setup_delete_section"
						   data-confirm="¿<?= __('Esta seguro que desea eliminar')?> '<?= $section->name ?>'?">
							<i class="fa fa-trash-o"></i>
						</a>
					</td>
				</tr>
				<?php endforeach; ?>
			</table>
			<ul class="pagination">
				<?= $this->Paginator->first('«') ?>
				<?= $this->Paginator->numbers() ?>
				<?= $this->Paginator->last('»') ?>
			</ul>
			<?php endif; ?>
        </div>
	</div>
</section>