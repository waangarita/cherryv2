<header class="main-header">

	<a href="<?= $this->Url->build(['controller' => 'Setup', 'action' => 'dashboard']) ?>" class="logo">
		<span class="logo-mini">G</span>
		<span class="logo-lg" style="line-height: 42px;"> <img src="<?= $this->Url->build('/images/cherry-logo.png') ?>" width="150px" alt=""> </span>
	</a>

	<nav class="navbar navbar-static-top" role="navigation">

		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>

		<div class="navbar-custom-menu">

			<ul class="nav navbar-nav">

				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<span class="hidden-xs"><?= $auth['first_name'] .' '.$auth['last_name'] ?></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header" style="height: auto !important;">
							<p>
								<?= $auth['first_name']  .' '.$auth['last_name']  ?>
							</p>
						</li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-right">
								<a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'logout']) ?>" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul>
				</li>

				<li>
					<a href="<?= $this->Url->build(['controller' => 'Auth', 'action' => 'logout']) ?>">
						<i class="fa fa-power-off text-red"></i>
						<span><?= __('Salir') ?></span>
					</a>
				</li>

			</ul>

		</div>

	</nav>
</header>
