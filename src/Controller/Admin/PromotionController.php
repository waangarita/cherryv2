<?php

namespace App\Controller\Admin;

class PromotionController extends AdminController {

	public function initialize () {
		parent::initialize();

		// load models
		$this->loadModel('TblPromotion');
		$this->loadModel('TblPromotionSlide');
	}

	/*
	* Promotion management
	*/

	public function index() {
		$promotions = $this->TblPromotion->find()->order(['section']);
		$this->set(compact('promotions'));
	}

	public function viewPromotion ($id) {
		$promotion = $this->TblPromotion->get($id);
		$detail_promotion = $this->TblPromotionSlide->find()->where(['id_promotion' => $id]);

		if ($this->request->is('post')) {
			$loguser = $this->Auth->user(); //logged user
			$promotion->name = $this->request->data["name"];
			$this->TblPromotion->save($promotion);

			/* se borran todos los registros que tengan ese id de seccion y luego se insertan los que se mantienen */
			$this->TblPromotionSlide->deleteAll(['id_promotion' => $id]);
			/* en la vista hay un hidden en donde guardo las rutas de las imagenes borradas para poder eliminarlas posteriormente */
			$img_delete = explode(',',$this->request->data['img_delete']);

			if(isset($this->request->data['slider'])) {
				try {
						$count_sliders = count($this->request->data['slider']);
					  for ($i = 0; $i<$count_sliders; $i++) {
							$slider = $this->TblPromotionSlide->newEntity();
							$slider->id_promotion = $id;
							$slider->cta = $this->request->data['url'][$i];

							/* img desktop */
							if (!empty($this->request->data['hidden'][$i])) {
								$slider->img_desktop = $this->request->data['hidden'][$i];
							} else {
								$file_path = '/files/sliders/desktop/' . $id . '_d_' . $i . '_' . date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['slider'][$i]['name'], PATHINFO_EXTENSION);
								$slider->img_desktop = $file_path;
							}

							/* img mobile */
							if(!empty($this->request->data['hidden_mobile'][$i])){
								$slider->img_mobile = $this->request->data['hidden_mobile'][$i];
							}else{
								$file_path_mobile = '/files/sliders/mobile/' . $id . '_m_' . $i . '_' .  date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['slider_mobile'][$i]['name'], PATHINFO_EXTENSION);
								$slider->img_mobile = $file_path_mobile;
							}

							if($this->TblPromotionSlide->save($slider)) {
								if(empty($this->request->data['hidden'][$i])){
									move_uploaded_file($this->request->data['slider'][$i]['tmp_name'], WWW_ROOT .  $file_path);
								}

								if(empty($this->request->data['hidden_mobile'][$i])){
									move_uploaded_file($this->request->data['slider_mobile'][$i]['tmp_name'], WWW_ROOT . $file_path_mobile);
								}
							}
						}

						// borrar imagenes que el usuario descarto cuando editó
						if(!empty($img_delete)) {
							for ($j = 0; $j < count($img_delete); $j++) {
								@unlink(WWW_ROOT . $img_delete[$j]);
							}
						}

						$successMsg = sprintf("Sliders guardados.");
						$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
						$this->Flash->success($successMsg);
						return $this->redirect(['action' => 'index']);

				} catch (\Exception $e) {
					$successMsg = sprintf("Error al guardar los Sliders.");
					$this->log($e->getMessage(), "error");
					$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'error');
					$this->Flash->error($successMsg);
					return $this->redirect(['action' => 'index']);
				}
			} else {
				//si borro todas las imagenes
				if(!empty($img_delete)) {
					for ($j = 0; $j < count($img_delete); $j++) {
						@unlink(WWW_ROOT . $img_delete[$j]);
					}
				}
				$successMsg = sprintf("Sliders guardados.");
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
				return $this->redirect(['action' => 'index']);
			}
		}

		$this->set(compact('promotion'));
		$this->set(compact('detail_promotion'));

	}
}

?>
