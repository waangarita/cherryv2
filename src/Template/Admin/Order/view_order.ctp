<?php
$this->assign('title', __('Orden Pedido'));
$this->assign('section', __('Orden Pedido'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?> #<?= $order->id ?>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Editar') ?></li>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-body">
      <form method="post" enctype="multipart/form-data">
        <table class="table table-condensed table-striped table-hover">
          <thead>
            <tr>
              <th width="30%"colspan="2">Producto</th>
              <th width="20%"> precio </th>
              <th width="10%"> Cantidad </th>
              <th width="15%"> Total </th>
            </tr>
          </thead>
          <tbody>
            <?php
            $total = 0;
            foreach ($detail_order AS $value) : ?>
              <tr>
                <td align="center"><img width="70px" src="/images/products/<?= h($value['img']) ?>" alt=""></td>
                <td>
                  <?= h($value['type_product']) ?>
                  <br>
                  <b><?= h($value['product_series']) ?></b>
                  <br><?= h($value['models']) ?>
                </td>
                <td>$ <?= h(number_format($value['price_product'],2)) ?></td>
                <td><?= h($value['amount']) ?></td>
                <td>$ <?= $value['amount'] * number_format($value['price_product'],2) ?></td>
              </tr>
            <?php
            $valor_productos = $value['amount'] * number_format($value['price_product'],2) ;
            $total = $total + $valor_productos;
            endforeach;
            ?>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="4"></td>
              <td> <h3>$ <?= h($total) ?></h3> </td>
            </tr>
          </tfoot>
        </table>
        <hr>
        <div class="form-group has-feedback">
          <label><?= __('Seleccione Status') ?></label>
          <select name="status" id="status" class="form-control">
            <option <?= $order->status == 'PENDING' ? 'selected' : '' ?> value="PENDING"><?= __('PENDIENTE') ?></option>
            <option <?= $order->status == 'OK' ? 'selected' : '' ?> value="OK"><?= __('APROBADO') ?></option>
            <option <?= $order->status == 'CANCEL' ? 'selected' : '' ?> value="CANCEL"><?= __('CANCELADO') ?></option>
          </select>
        </div>
        <hr>
        <div class="row">
          <div class="col-xs-4">
            <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-danger">
              <?= __('Cancelar') ?>
            </a>
          </div>
          <div class="col-xs-offset-4 col-xs-4 text-right">
            <button type="submit" class="btn btn-primary order_edit_order"><?= __('Guardar') ?></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
