<?php
$this->assign('title', __('Preguntas'));
$this->assign('section', __('Preguntas'));
?>
<section class="content-header">
	<h1>
		<?= $this->fetch('section') ?>
		<small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de preguntas') ?></h3>
			<a href="<?= $this->Url->build(['action' => 'add_faq']) ?>" class="btn btn-default btn-sm faq_add_faq" title="Agregar" style="float: right;">
				<i class="fa fa-plus"></i>
			</a>
		</div>
		<div class="box-body">
			<?php if( $faq->count() == 0 ): ?>
				<div class="alert alert-info">
					<?= __('No hay preguntas para mostrar.') ?>
				</div>
			<?php else: ?>
				<table class="table table-condensed table-bordered dataTables table-striped table-hover">
					<thead>
						<tr>
							<th><?= __('Preguntas') ?></th>
							<th style="text-align: center;"><?= __('Fecha actualizacion') ?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($faq as $fq): ?>
							<tr>
								<td>
									<?= h($fq->question) ?>
								</td>
								<td align="center">
									<?= h($fq->created->format('m/d/y h:i')) ?>
								</td>
								<td class="text-right">
									<a href="<?= $this->Url->build(['action' => 'edit_faq', $fq->id]) ?>" class="btn btn-warning btn-sm faq_edit_faq">
										<i class="fa fa-edit"></i>
									</a>
									<a href="<?= $this->Url->build(['action' => 'delete_faq', $fq->id]) ?>" class="btn btn-danger btn-sm faq_delete_faq"
										onclick="return confirm('<?= __("¿Esta seguro que desea eliminar $fq->question ")  ?>?');">
										<i class="fa fa-trash-o"></i>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	</div>
</section>
