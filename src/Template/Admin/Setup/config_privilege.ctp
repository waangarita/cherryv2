<?php
$this->assign('title', __('Configuracion'));
$this->assign('section', __('Configuracion'));
?>
<section class="content-header">
    <h1>
        <?= $this->fetch('section') ?>
        <small><?= __('Rol') ?>: <?= $rol->name ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?= $this->Url->build(['action' => 'indexSection']) ?>">
                <?= $this->fetch('section') ?>
            </a>
        </li>
        <li class="active"><?= __('Crear') ?></li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-body">
            <form method="post" enctype="multipart/form-data">
            	<?php if(!empty($sections)){ ?>	            	
                    <table class="table table-condensed table-striped table-hover">
                        <tr>
                            <th><?php echo $this->Paginator->sort('name', 'Sección'); ?></th>
                            <th style="text-align: center;"><?= __('Habilitado') ?></th>
                        </tr>
                        <?php foreach($sections as $index => $section): ?>
                        <tr>
                            <td>
                                <?= h($section->name) ?>
                            </td>
                            <td style="text-align: center;">
                                <input type="checkbox" name="<?= $section->id ?>_privilege" class="privilege-check" value="enabled" <?php foreach ($privileges as $privile){ if($privile->section_id == $section->id){echo 'checked';}} ?>>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                    <ul class="pagination">
                        <?= $this->Paginator->first('«') ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->last('»') ?>
                    </ul>
            	<?php } ?>
                <hr>
                <div class="row">
                    <div class="col-xs-4">
                        <a href="<?= $this->Url->build(['action' => 'indexPrivilege']) ?>" class="btn btn-danger">
                            <?= __('Cancelar') ?>
                        </a>
                    </div>
                    <div class="col-xs-offset-4 col-xs-4 text-right">
                        <button type="submit" class="btn btn-primary setup_config_privilege"><?= __('Guardar') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<?php $this->append('scripts'); ?>
<script type="text/javascript">
var privilegesData = { dataArr:[] };

$(document).ready(function(){
    $('.privilege-check').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });
});

$('.categories-child').on('ifChecked', function(event){
    var jsonData = {};
    jsonData['section_id'] = $('#section_id').val();
    jsonData['privilege'] = $(this).val();
    privilegesData.dataArr.push(jsonData);
});
</script>
<?php $this->end(); ?>
