<?php
$this->assign('title', __('Paginas'));
$this->assign('section', __('Paginas'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Editar') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Editar') ?></li>
  </ol>
</section>
<form method="post" enctype="multipart/form-data">
  <input type="hidden" id="img_delete" name="img_delete" value="">
  <section class="content">
    <div class="box box-primary">
      <div class="box-body">
        <div class="form-group has-feedback">
          <label>Nombre</label>
          <input type="text" name="name" value="<?= $site->name ?>" readonly class="form-control" placeholder="Nombre completo">
        </div>
        <hr class="separator">
      </div>
      <div class="box-body">
        <textarea  id="textbody"  name="body" rows="8" ><?= $site->body ?></textarea>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-4">
        <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-danger">
          <?= __('Cancelar') ?>
        </a>
      </div>
      <div class="col-xs-offset-4 col-xs-4 text-right">
        <button type="submit" class="btn btn-primary promotion_edit_promotion"><?= __('Guardar') ?></button>
      </div>
    </div>
  </section>

</form>

<?php $this->append('scripts'); ?>
<script>
  tinymce.init({
    height : '480',
    menubar: false,
    selector:'#textbody',
    plugins: "textcolor colorpicker media jbimages link",
    toolbar: 'slider | removeformat | styleselect | bold italic | alignleft aligncenter alignright alignjustify | forecolor backcolor | bullist numlist | jbimages media link',
    setup: function (editor) {
      editor.addButton('slider', {
        text: 'Add Slider',
        icon: false,
        onclick: function () {
          editor.insertContent('[slider]');
        }
      });
    },
    relative_urls: false
  });
</script>
<?php $this->end(); ?>
