<?php
$this->assign('title', __('Lista precios'));
$this->assign('section', __('Lista precios'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Listado') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Listado') ?></li>
  </ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= __('Lista de precios') ?> <?= $list_price->name ?></h3>
		</div>

		<div class="box-body">
			<?php if( $detail_list->count() == 0 ): ?>
				<div class="alert alert-info">
					<?= __('No hay lista de precios para mostrar.') ?>
				</div>
			<?php else: ?>
				<table class="table table-bordered dataTables table-condensed table-striped table-hover">
					<thead>
						<tr>
							<th><?= __('Codigo'); ?></th>
							<th><?= __('Tipo producto'); ?></th>
							<th><?= __('Precio'); ?></th>
							<th style="text-align: center;"><?= __('Fecha creacion'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($detail_list as $detail): ?>
							<tr>
								<td>
									<?= h($detail->tbl_product->code) ?>
								</td>
								<td>
									<?= h($detail->tbl_product->type_product) ?>
								</td>
								<td>
									$ <?= h($detail->price) ?>
								</td>
								<td align="center">
									<?= h($detail->created->format('m/d/y h:i')) ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	</div>
</section>