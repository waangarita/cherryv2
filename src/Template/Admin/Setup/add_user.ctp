<?php
$this->assign('title', __('Usuarios'));
$this->assign('section', __('Usuarios'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Crear') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'indexUser']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Crear') ?></li>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= __('Crear') ?></h3>
    </div>
    <div class="box-body">
      <form method="post" autocomplete="off">
        <div class="form-group has-feedback">
          <label><?= __('Nombres') ?> <span class="text-danger">*</span></label>
          <input type="text" name="first_name" class="form-control" placeholder="<?= __('Nombres') ?>" required="true">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Apellidos') ?> <span class="text-danger">*</span></label>
          <input type="text" name="last_name" class="form-control" placeholder="<?= __('Apellidos') ?>" required="true">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Cargo') ?> <span class="text-danger">*</span> </label>
          <input type="text" name="appoinment" class="form-control" placeholder="<?= __('Cargo') ?>" required="true">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Seleccionar pais') ?> <span class="text-danger">*</span></label>
          <select name="country_id" class="form-control">
            <?php foreach ($countries as $country): ?>
              <option  value="<?= $country->id ?>">(<?= $country->phone_code ?>) <?= $country->name ?> </option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Telefono 1') ?> <span class="text-danger">*</span></label>
          <input type="text" name="phone1" class="form-control" placeholder="<?= __('Telefono 1') ?>" required="true">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Telefono 2') ?></label>
          <input type="text" name="phone2" class="form-control" placeholder="<?= __('Telefono 2') ?>" >
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Email') ?> <span class="text-danger">*</span></label>
          <input type="email" name="email" class="form-control"  placeholder="<?= __('Email') ?>" required="true">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
          <label><?= __('Contraseña') ?> <span class="text-danger">*</span></label>
          <input type="password" id="password" name="password" class="form-control" placeholder="<?= __('Contraseña') ?>" value="">
          <a href="#" class="btn btn-primary" onclick="generatePassw(); return false;" style="margin-top: 10px;"><?= __('Generar contraseña') ?></a>
          <span id="generated_passw"></span>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Seleccionar cliente') ?> <span class="text-danger">*</span></label>
          <select name="id_client" class="form-control">
            <?php foreach ($clients as $client) { ?>
              <option value="<?= $client->code ?>"><?= $client->name ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group has-feedback">
            <label><?= __('Seleccionar rol') ?> <span class="text-danger">*</span></label>
            <select name="role_id" class="form-control">
              <?php foreach ($roles as $role) { ?>
                <option value="<?= $role->id?>"><?= $role->name ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group has-feedback">
              <label><?= __('Seleccione estado') ?> <span class="text-danger">*</span></label>
              <select name="status" class="form-control">
                <option value="STANDBY"><?= __('Standby') ?></option>
                <option value="ACTIVE"><?= __('Activo') ?></option>
                <option value="BLOCKED"><?= __('Bloqueado') ?></option>
                <option value="DELETED"><?= __('Eliminado') ?></option>
              </select>
            </div>
            <div class="row">
              <div class="col-xs-4">
                <a href="<?= $this->Url->build(['action' => 'indexUser']) ?>" class="btn btn-danger">
                  <?= __('Cancelar') ?>
                </a>
              </div>
              <div class="col-xs-offset-4 col-xs-4 text-right">
                <button type="submit" class="btn btn-primary setup_add_user"><?= __('Guardar') ?></button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>

    <?php $this->append('scripts'); ?>
    <script type="text/javascript">
    function generatePassw(){
      var password = Math.random().toString(36).slice(-8);
      $('#password').val(password);
      $('#generated_passw').html("Contraseña: " + password);
    }
    </script>
    <?php $this->end(); ?>
