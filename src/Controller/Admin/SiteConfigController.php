<?php

namespace App\Controller\Admin;

class SiteConfigController extends AdminController {

	public function initialize () {
		parent::initialize();

		// load models
		$this->loadModel('TblSiteConfig');
		$this->loadModel('TblSiteConfigSlide');
	}

	/*
	* Promotion management
	*/

	public function index () {
		$sites = $this->TblSiteConfig->find()->order(['name ASC']);
		$this->set(compact('sites'));
	}

	public function viewSite ($id) {
		$site = $this->TblSiteConfig->get($id);
		$slides = $this->TblSiteConfigSlide->find()->where(['id_site' => $id]);

		if ($this->request->is('post')) {
			$loguser = $this->Auth->user(); //logged user
			$site->name = $this->request->data["name"];

			// OJO QUE ESTAS HACIENDO AQUI
			$site->slide_mobile1 = $this->request->data["slider_mobile1"];
			$site->slide_mobile2 = $this->request->data["slider_mobile2"];
			$site->slide_mobile3 = $this->request->data["slider_mobile3"];
			// END

			$site->cta1 = $this->request->data["url1"];
			$site->cta2 = $this->request->data["url2"];
			$site->cta3 = $this->request->data["url3"];
			$site->cta_banner = $this->request->data["url_banner"];
			$site->body = '';


			/* se borran todos los registros que tengan ese id de seccion y luego se insertan los que se mantienen */
			$this->TblSiteConfigSlide->deleteAll(['id_site' => $id]);
			/* en la vista hay un hidden en donde guardo las rutas de las imagenes borradas para poder eliminarlas posteriormente */
			$img_delete = explode(',',$this->request->data['img_delete']);

			/* img central desktop */
			if (!empty($this->request->data['hidden1'])) {
				$site->slide1 = $this->request->data['hidden1'];
			} else {
				$file_path1 = '/files/sliders/desktop/' . $id . '_d1_' . date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['slider1']['name'], PATHINFO_EXTENSION);
				$site->slide1 = $file_path1;

			}

			if (!empty($this->request->data['hidden2'])) {
				$site->slide2 = $this->request->data['hidden2'];
			} else {
				$file_path2 = '/files/sliders/desktop/' . $id . '_d2_' . date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['slider2']['name'], PATHINFO_EXTENSION);
				$site->slide2 = $file_path2;
			}

			if (!empty($this->request->data['hidden3'])) {
				$site->slide3= $this->request->data['hidden3'];
			} else {
				$file_path3 = '/files/sliders/desktop/' . $id . '_d3_' . date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['slider3']['name'], PATHINFO_EXTENSION);
				$site->slide3 = $file_path3;
			}

			if (!empty($this->request->data['banner_hidden'])) {
				$site->banner = $this->request->data['banner_hidden'];
			} else {
				$file_path_banner = '/files/sliders/desktop/' . $id . '_banner_' . date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['banner']['name'], PATHINFO_EXTENSION);
				$site->banner = $file_path_banner;
			}

			/* img central mobile */

			if(!empty($this->request->data['hidden_mobile1'])){
				$site->slide_mobile1 = $this->request->data['hidden_mobile1'];
			}else{
				$file_path_mobile1 = '/files/sliders/mobile/' . $id . '_m1_' .  date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['slider_mobile1']['name'], PATHINFO_EXTENSION);
				$site->slide_mobile1 = $file_path_mobile1;
			}

			if(!empty($this->request->data['hidden_mobile2'])){
				$site->slide_mobile2 = $this->request->data['hidden_mobile2'];
			}else{
				$file_path_mobile2 = '/files/sliders/mobile/' . $id . '_m2_' .  date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['slider_mobile2']['name'], PATHINFO_EXTENSION);
				$site->slide_mobile2 = $file_path_mobile2;
			}

			if(!empty($this->request->data['hidden_mobile3'])){
				$site->slide_mobile3 = $this->request->data['hidden_mobile3'];
			}else{
				$file_path_mobile3 = '/files/sliders/mobile/' . $id . '_m3_' .  date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['slider_mobile3']['name'], PATHINFO_EXTENSION);
				$site->slide_mobile3 = $file_path_mobile3;
			}

			if(!empty($this->request->data['banner_hidden_mobile'])){
				$site->banner_mobile = $this->request->data['banner_hidden_mobile'];
			}else{
				$file_path_mobile_banner = '/files/sliders/mobile/' . $id . '_banner_m_' .  date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['banner_mobile']['name'], PATHINFO_EXTENSION);
				$site->banner_mobile = $file_path_mobile_banner;
			}

			if($this->TblSiteConfig->save($site)) {
				if(empty($this->request->data['hidden1'])){
					move_uploaded_file($this->request->data['slider1']['tmp_name'], WWW_ROOT .  $file_path1);
				}

				if(empty($this->request->data['hidden2'])){
					move_uploaded_file($this->request->data['slider2']['tmp_name'], WWW_ROOT .  $file_path2);
				}

				if(empty($this->request->data['hidden3'])){
					move_uploaded_file($this->request->data['slider3']['tmp_name'], WWW_ROOT .  $file_path3);
				}

				if(empty($this->request->data['banner_hidden'])){
					move_uploaded_file($this->request->data['banner']['tmp_name'], WWW_ROOT .  $file_path_banner);
				}


				//

				if(empty($this->request->data['hidden_mobile1'])) {
					move_uploaded_file($this->request->data['slider_mobile1']['tmp_name'], WWW_ROOT . $file_path_mobile1);
				}

				if(empty($this->request->data['hidden_mobile2'])) {
					move_uploaded_file($this->request->data['slider_mobile2']['tmp_name'], WWW_ROOT . $file_path_mobile2);
				}

				if(empty($this->request->data['hidden_mobile3'])) {
					move_uploaded_file($this->request->data['slider_mobile3']['tmp_name'], WWW_ROOT . $file_path_mobile3);
				}

				if(empty($this->request->data['banner_hidden_mobile'])) {
					move_uploaded_file($this->request->data['banner_mobile']['tmp_name'], WWW_ROOT . $file_path_mobile_banner);
				}

			}


			if(isset($this->request->data['slider'])) {
				try {
						$count_sliders = count($this->request->data['slider']);
					  for ($i = 0; $i<$count_sliders; $i++) {
							$slider = $this->TblSiteConfigSlide->newEntity();
							$slider->id_site = $id;
							$slider->cta = $this->request->data['url'][$i];

							/* img desktop */
							if (!empty($this->request->data['hidden'][$i])) {
								$slider->img_desktop = $this->request->data['hidden'][$i];
							} else {
								$file_path = '/files/sliders/desktop/' . $id . '_d_' . $i . '_' . date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['slider'][$i]['name'], PATHINFO_EXTENSION);
								$slider->img_desktop = $file_path;
							}

							/* img mobile */
							if(!empty($this->request->data['hidden_mobile'][$i])){
								$slider->img_mobile = $this->request->data['hidden_mobile'][$i];
							}else{
								$file_path_mobile = '/files/sliders/mobile/' . $id . '_m_' . $i . '_' .  date('Y-m-d_h:i:s').'.'.pathinfo($this->request->data['slider_mobile'][$i]['name'], PATHINFO_EXTENSION);
								$slider->img_mobile = $file_path_mobile;
							}

							if($this->TblSiteConfigSlide->save($slider)) {
								if(empty($this->request->data['hidden'][$i])){
									move_uploaded_file($this->request->data['slider'][$i]['tmp_name'], WWW_ROOT .  $file_path);
								}

								if(empty($this->request->data['hidden_mobile'][$i])) {
									move_uploaded_file($this->request->data['slider_mobile'][$i]['tmp_name'], WWW_ROOT . $file_path_mobile);
								}
							}
						}

						// borrar imagenes que el usuario descarto cuando editó
						if(!empty($img_delete)) {
							for ($j = 0; $j < count($img_delete); $j++) {
								@unlink(WWW_ROOT . $img_delete[$j]);
							}
						}

						$successMsg = sprintf("Sliders guardados.");
						$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
						$this->Flash->success($successMsg);
						return $this->redirect(['action' => 'index']);

				} catch (\Exception $e) {
					$successMsg = sprintf("Error al guardar los Sliders.");
					$this->log($e->getMessage(), "error");
					$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'error');
					$this->Flash->error($successMsg);
					return $this->redirect(['action' => 'index']);
				}
			} else {
				// borro todas las imagenes
				if(!empty($img_delete)) {
					for ($j = 0; $j < count($img_delete); $j++) {
						@unlink(WWW_ROOT . $img_delete[$j]);
					}
				}
				$successMsg = sprintf("Sliders guardados.");
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
				return $this->redirect(['action' => 'index']);
			}
		}

		$this->set(compact('site'));
		$this->set(compact('slides'));
	}

	public function viewTerms ($id) {
		$site = $this->TblSiteConfig->get($id);

		if ($this->request->is('post')) {
			$loguser = $this->Auth->user(); //logged user
			$site->name = $this->request->data["name"];
			$site->slide_mobile1 = '';
			$site->slide_mobile2 = '';
			$site->slide_mobile3 = '';
			$site->cta1 = '';
			$site->cta2 = '';
			$site->cta3 = '';
			$site->banner = '';
			$site->cta_banner = '';
			$site->body = $this->request->data["body"];
			if($this->TblSiteConfig->save($site)) {
				$successMsg = sprintf("Sliders guardados.");
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
				return $this->redirect(['action' => 'index']);
			}
		}

		$this->set(compact('site'));
		$this->set(compact('slides'));
	}
}

?>
