<?php
$this->assign('title', __('Preguntas'));
$this->assign('section', __('Preguntas'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Editar') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Editar') ?></li>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-body">
      <form method="post" enctype="multipart/form-data">
        <div class="form-group has-feedback">
          <label><?= __('Pregunta') ?></label>
          <input type="text" name="question" class="form-control" placeholder="<?= __('Pregunta') ?>" value="<?= $faq->question ?>" required="true">
          <span class="form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
          <label><?= __('Descripcion') ?></label>
          <textarea name="answer"  class="form-control" rows="4"><?= h($faq->answer) ?></textarea>
        </div>
        <hr>
        <div class="row">
          <div class="col-xs-4">
            <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-danger">
              <?= __('Cancelar') ?>
            </a>
          </div>
          <div class="col-xs-offset-4 col-xs-4 text-right">
            <button type="submit" class="btn btn-primary faq_edit_faq"><?= __('Guardar') ?></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
