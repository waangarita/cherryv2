<?php

use Phinx\Migration\AbstractMigration;

class CreateTblFamily extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
      $tableBrand = $this->table('tbl_brand', ['id' => false, 'primary_key' => 'code']);
      $tableBrand ->  addColumn('code', 'string')
                  ->  addColumn('name', 'string')
                  ->  addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP', 'null' => 'true' ))
                  ->  addColumn('modified', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
                  ->  create();

      $table = $this->table('tbl_family', ['id' => false, 'primary_key' => 'code']);
      $table  ->  addColumn('code', 'string')
              ->  addColumn('name', 'string')
              ->  addColumn('description', 'text')
              ->  addColumn('id_brand', 'string')
              ->  addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP', 'null' => 'true' ))
              ->  addColumn('modified', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
              ->  create();

      $refTable = $this->table('tbl_family');
      $refTable ->  addForeignKey('id_brand', 'tbl_brand', 'code', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
                ->  update();
    }
}
