<?php

namespace App\Controller\Admin;

class FaqController extends AdminController {

	public function initialize()
	{
		parent::initialize();
		// load models
		$this->loadModel('TblFaq');
	}

	/*
	* Family management
	*/

	public function index() {
		$faq = $this->TblFaq->find()->order(['question ASC']);
		$this->set(compact('faq'));
	}

	public function addFaq() {
  	if($this->request->is('post'))
		{
			$loguser = $this->Auth->user(); //logged user
			$faq = $this->TblFaq->newEntity($this->request->data());

			if($this->TblFaq->save($faq))
			{
				$successMsg = sprintf("Faq '%s' creada.", $faq->question);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success('La faq ha sido creada correctamente.');
				return $this->redirect(['action' => 'index']);
			}
			else
			{
				$successMsg = sprintf("Ha ocurrido un error Creando la '%s'.", $faq->question);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$faq->errors();
				$this->Flash->error('La faq no pudo ser creada. Por favor, intente nuevamente.');
			}
		}

    $this->set(compact('faq'));
	}

	public function editFaq ($id) {
		$faq = $this->TblFaq->get($id);
    $this->set(compact('faq'));

		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			$faq = $this->TblFaq->patchEntity($faq, $this->request->data() );
			if( $this->TblFaq->save($faq) ) {
				$successMsg = sprintf("Faq '%s' editada.", $faq->question);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
			} else {
				$failMsg = 'Ha ocurrido un error editado la faq. ';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($faq->errors(), "error");
				$this->Flash->error($failMsg);
			}
			return $this->redirect(['action' => 'index']);
		}
	}

	public function deleteFaq ($id) {
		$faq = $this->TblFaq->get($id);
		$faqName = $faq->question;
    $loguser = $this->Auth->user(); //logged user
    try{
        if( $this->TblFaq->delete($faq) ) {
            $successMsg = sprintf("Faq eliminada. [%s]", $faqName);
             $this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
            $this->Flash->success($successMsg);
            return $this->redirect(['action' => 'index']);
        }
    } catch (\Exception $e) {
        $failMsg = 'Error al eliminar la faq';
        $this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
        $this->Flash->error($failMsg);
        return $this->redirect(['action' => 'index']);
    }
	}
}

?>
