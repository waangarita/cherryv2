<?php
$this->assign('title', __('Preguntas'));
$this->assign('section', __('Preguntas'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Crear') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Crear') ?></li>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= __('Crear') ?></h3>
    </div>
    <div class="box-body">
      <form method="post" autocomplete="off">
        <div class="form-group has-feedback">
          <label><?= __('Pregunta') ?></label>
          <input type="text" required="true" name="question" class="form-control" placeholder="<?= __('Pregunta') ?>">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Respuesta') ?></label>
          <textarea name="answer" class="form-control" rows="4"></textarea>
        </div>
        <div class="row">
          <div class="col-xs-4">
            <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-danger">
              <?= __('Cancelar') ?>
            </a>
          </div>
          <div class="col-xs-offset-4 col-xs-4 text-right">
            <button type="submit" class="btn btn-primary faq_add_faq"><?= __('Guardar') ?></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>

<?php $this->append('scripts'); ?>
<script type="text/javascript">

</script>
<?php $this->end(); ?>
