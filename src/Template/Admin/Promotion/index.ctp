<?php
$this->assign('title', __('Promociones'));
$this->assign('section', __('Promociones'));
?>
<section class="content-header">
	<h1>
		<?= $this->fetch('section') ?>
		<small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de promociones') ?></h3>
		</div>
		<div class="box-body">
			<?php if( $promotions->count() == 0 ): ?>
				<div class="alert alert-info">
					<?= __('No hay promociones para mostrar.') ?>
				</div>
			<?php else: ?>
				<table class="table table-condensed table-bordered dataTables table-striped table-hover">
					<thead>
						<tr>
							<th><?= __('Nombre') ?></th>
							<th style="text-align: center;"><?= __('Fecha actualizacion') ?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($promotions as $promotion): ?>
							<tr>
								<td>
									<?= h($promotion->name) ?>
								</td>
								<td align="center">
									<?= h($promotion->created->format('m/d/y h:i')) ?>
								</td>
								<td class="text-right">
									<a href="<?= $this->Url->build(['action' => 'view_promotion', $promotion->id]) ?>" class="btn btn-warning btn-sm promotion_view_promotion">
										<i class="fa fa-edit"></i>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

			<?php endif; ?>
		</div>
	</div>
</section>
