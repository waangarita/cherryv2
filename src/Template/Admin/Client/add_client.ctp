<?php
$this->assign('title', __('Clientes'));
$this->assign('section', __('Clientes'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Crear') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Crear') ?></li>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><?= __('Crear') ?></h3>
    </div>
    <div class="box-body">
      <form method="post" autocomplete="off">
        <div class="form-group has-feedback">
          <label><?= __('Còdigo ERP') ?> <span class="text-danger">*</span></label>
          <input type="text" name="code" required="true" class="form-control" placeholder="<?= __('Còdigo') ?>">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Nombre completo') ?> <span class="text-danger">*</span></label>
          <input type="text" required="true" name="name" class="form-control" placeholder="<?= __('Nombre completo') ?>">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Seleccione tipo lista de precios') ?> <span class="text-danger">*</span></label>
          <select name="id_type_list_price" id="id_type_list_price" required="true" class="form-control">
            <?php foreach($tpListPrice as $listPrice): ?>
              <option value="<?= h($listPrice->name) ?>"><?= h($listPrice->name) ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      <div class="row">
        <div class="col-xs-4">
          <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-danger">
            <?= __('Cancelar') ?>
          </a>
        </div>
        <div class="col-xs-offset-4 col-xs-4 text-right">
          <button type="submit" class="btn btn-primary client_add_client"><?= __('Guardar') ?></button>
        </div>
      </div>
    </form>
  </div>
</div>
</section>
