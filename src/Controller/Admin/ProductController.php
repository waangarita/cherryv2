<?php

namespace App\Controller\Admin;

class ProductController extends AdminController {

	public function initialize () {
		parent::initialize();

		// load models
		$this->loadModel('TblProduct');
		$this->loadModel('TblFamily');
		$this->loadModel('TblCrossReferences');
		$this->loadModel('TblFileUpload');
		$this->loadModel('TblReportCross');
		$this->loadModel('TblReportProduct');
	}

	/*
	* Products management
	*/

	public function index($hash = null) {
		$this->huerfanos();
		if (isset($this->request->query['cross'])) {
			$reports = $this->TblReportCross->find();
			$reports->select([
				'cuantos' => $reports->func()->count('motivo'),
				'motivo',
				])->group('motivo');

				$this->log($reports,"debug");

				$errores = $this->TblReportCross->find()->select(['cuantos' => $reports->func()->count('motivo')]);

				$file = $this->TblFileUpload->find()->where(['hash' => $this->request->query['cross'] ]);

				$this->set(compact('errores'));
				$this->set(compact('file'));
				$this->set(compact('reports'));
		}

		if (isset($this->request->query['product'])) {
			$reports = $this->TblReportProduct->find();
			$reports->select([
				'cuantos' => $reports->func()->count('motivo'),
				'motivo',
			])->group('motivo');

			$this->log($reports,"debug");

			$errores = $this->TblReportProduct->find()->select(['cuantos' => $reports->func()->count('motivo')]);

			$file = $this->TblFileUpload->find()->where(['hash' => $this->request->query['product'] ]);

			$this->set(compact('errores'));
			$this->set(compact('file'));
			$this->set(compact('reports'));
		}

		$products = $this->TblProduct->find()
		->contain(['TblFamily' => function ($q) {
			return $q->contain(['TblBrand'=> function ($q) {
				return $q->select(['name']);
			}])->select(['code', 'name']);
		}])
		->where(['active' => 1])
		->order(['type_product ASC']);
		$this->set(compact('products'));
	}

	public function huerfanos () {
		$lista = [];
		$lista[] = array('Producto huerfano');
		$huerfanos = $this->TblProduct->huerfanos();

		foreach ($huerfanos AS $value) {
			$lista[] = array($value['id_product_related']);
		}

		$fp = fopen( WWW_ROOT . 'files' . DS . 'example/huerfanas.csv', 'w');

		foreach ($lista as $campos) {
		  fputcsv($fp, $campos);
		}

		fclose($fp);
	}

	public function addProduct() {
		$family = $this->TblFamily->find()
		->contain(['TblBrand' => function ($q) {
			return $q->select(['name']);
		}])
		->select(['name', 'code']);
		if($this->request->is('post'))
		{
			$exists = $this->TblProduct->verifyProduct($this->request->data('code'));

			if($exists) {
				$this->Flash->warning('Ya existe un producto con este codigo, no puedo crearlo');
			} else {
				$loguser = $this->Auth->user(); //logged user
				$product = $this->TblProduct->newEntity($this->request->data());
				$product->code = $this->request->data('code');
				$product->img = $this->request->data('code').'.jpg';
				$product->active = true;

				if($this->TblProduct->save($product))
				{
					$successMsg = sprintf("Producto '%s' creado.", $product->type_product);
					$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
					$this->Flash->success('El producto ha sido creado correctamente.');
					return $this->redirect(['action' => 'index']);
				}
				else
				{
					$successMsg = sprintf("Ha ocurrido un error Creando el '%s' ", $product->type_product);
					$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
					$product->errors();
					$this->Flash->error('El producto no pudo ser creado. Por favor, intente nuevamente.');
				}
			}
		}

		$this->set('families', $family);
		$this->set(compact('product'));
	}

	public function editProduct ($id) {
		$query = $this->TblCrossReferences->find()
		->contain(['TblProduct' => function ($q) {
			return $q->select(['code', 'type_product','models','status']);
		}])
		->where(['id_product' => $id ]);

		$cross_references = $this->Paginator->paginate($query,['limit' => 100]);
		$product = $this->TblProduct->get($id,['contain' => 'TblFamily']);
		$this->set(compact('product'));
		$this->set(compact('cross_references'));

		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			$product = $this->TblProduct->patchEntity($product, $this->request->data());
			$product->code = $this->request->data('code');
			$product->active = true;

			if( $this->TblProduct->save($product) ) {
				$successMsg = sprintf("Producto '%s' editado.", $product->type_product);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
			} else {
				$failMsg = 'Ha ocurrido un error editando el producto. ';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($family->errors(), "error");
				$this->Flash->error($failMsg);
			}
			return $this->redirect(['action' => 'index']);
		}
	}

	public function deleteProduct ($id) {
		$product = $this->TblProduct->get($id);
		$productType = $product->type_product;
		$loguser = $this->Auth->user(); //logged user
		try{
			if( $this->TblProduct->delete($product) ) {
				$successMsg = sprintf("Producto eliminado. [%s]", $productType);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
				return $this->redirect(['action' => 'index']);
			}
		} catch (\Exception $e) {
			$failMsg = 'Error al eliminar el producto';
			$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
			$this->Flash->error($failMsg);
			return $this->redirect(['action' => 'index']);
		}
	}

	public function uploadProduct () {
		if($this->request->is('post')) {
			$loguser = $this->Auth->user(); //logged user
			$file = $_FILES['file'];
			// Check if directory exists make sure it has correct permissions, if not make it
			if (is_dir($destination_directory = WWW_ROOT . 'files' . DS . 'uploads_products')) {
				chmod($destination_directory, 0775);
			} else {
				mkdir($destination_directory, 0775, true);
			}

			$filename = $file['name'];
			$csvFile = $destination_directory . DS . $file['name'];

			//upload csv file
			if (move_uploaded_file($file['tmp_name'], $csvFile)) {
				$this->TblReportProduct->deleteAll(array('id > ' => 0));
				$this->TblProduct->updateAll(array('active' => 0), array('active' => 1));
				$hoy = date('y-m-d h:i');
				$hash = md5($hoy.$csvFile);

				if ($this->TblProduct->loadDataLocalInfile($csvFile, $hash)) {
					$this->Flash->success('Productos cargados con éxito!');
				} else {
					$this->Flash->error('Ha ocurrido un error al cargar los productos , por favor verifique que la linea de cada producto se encuentre creada');
				}
			}
		}
		return $this->redirect(['action' => 'index', 'product' => $hash ]);
	}

	public function uploadCrossRefereces () {
		if($this->request->is('post')) {
			$loguser = $this->Auth->user(); //logged user
			$file = $_FILES['file_cross'];
			// Check if directory exists make sure it has correct permissions, if not make it
			if (is_dir($destination_directory = WWW_ROOT . 'files' . DS . 'uploads_references')) {
				chmod($destination_directory, 0775);
			} else {
				mkdir($destination_directory, 0775, true);
			}

			$filename = $file['name'];
			$csvFile = $destination_directory . DS . $file['name'];

			//upload csv file
			if (move_uploaded_file($file['tmp_name'], $csvFile)) {
				$this->TblCrossReferences->deleteAll(array('id_product <> ' =>  0));
				$this->TblReportCross->deleteAll(array('product <> ' =>  0));
				$hoy = date('y-m-d h:i');
				$hash = md5($hoy.$csvFile);

				if ($this->TblProduct->loadDataCrossReferences($csvFile, $hash)) {
					$this->Flash->success('Cross References cargado con éxito!');
				} else {
					$this->Flash->error('Ha ocurrido un error al cargar cross references, por favor verifique que todos los productos se encuentren creados');
				}
			}
		}
		return $this->redirect(['action' => 'index', 'cross' => $hash ]);
	}
}
?>
