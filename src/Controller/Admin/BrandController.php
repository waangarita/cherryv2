<?php

namespace App\Controller\Admin;

class BrandController extends AdminController {

	public function initialize()
	{
		parent::initialize();

		// load models
		$this->loadModel('TblBrand');
	}

	/*
	* Brand management
	*/

	public function index() {
		$brands = $this->TblBrand->find()->select(['code', 'name', 'created']);
		$this->set('brands', $brands);
	}

	public function addBrand() {
		if($this->request->is('post'))
		{
			$exists = $this->TblBrand->verifyBrand($this->request->data('code'));

			if($exists) {
				$this->Flash->warning('Ya existe una marca con este codigo , no es posible crearla');
			}else {
				$loguser = $this->Auth->user(); //logged user
				$brand = $this->TblBrand->newEntity($this->request->data());
				$brand->code = $this->request->data('code');

				if($this->TblBrand->save($brand))
				{
					$successMsg = sprintf("Marca '%s' creada.", $brand->name);
					$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
					$this->Flash->success('La marca ha sido creada correctamente.');
					return $this->redirect(['action' => 'index']);
				}
				else
				{
					$successMsg = sprintf("Ha ocurrido un error creando la '%s' creada.", $brand->name);
					$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
					$brand->errors();
					$this->Flash->error('La marca no pudo ser creada. Por favor, intente nuevamente.');
				}
			}
		}
		$this->set(compact('brand'));
	}

	public function editBrand ($id) {
		$brand = $this->TblBrand->get($id);
		$this->set('brand', $brand);

		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			$brand = $this->TblBrand->patchEntity($brand, $this->request->data());

			if ($this->TblBrand->save($brand)) {
				$successMsg = sprintf("Marca '%s' editada.", $brand->name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
			} else {
				$failMsg = 'Ha ocurrido un error editando la marca. ';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($brand->errors(), "error");
				$this->Flash->error($failMsg);
			}
			return $this->redirect(['action' => 'index']);
		}
	}

	public function deleteBrand ($id) {
		$brand = $this->TblBrand->get($id);
		$brandName = $brand->name;
		$loguser = $this->Auth->user(); //logged user
		try{
			if( $this->TblBrand->delete($brand) ) {
				$successMsg = sprintf("Marca eliminada. [%s]", $brandName);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
				return $this->redirect(['action' => 'index']);
			}
		} catch (\Exception $e) {
			$failMsg = 'Error al eliminar la marca';
			$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
			$this->Flash->error($failMsg);
			return $this->redirect(['action' => 'index']);
		}
	}
}

?>
