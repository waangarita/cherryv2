<?php
$this->assign('title', __('Dashboard'));
$this->assign('section', __('Dashboard'));
?>
<section class="content-header">
    <h1>
        <?= $this->fetch('section') ?>
        <small><?= __('Dashboard') ?></small>
    </h1>
</section>
<form method="post">
  <section class="row" style="margin-top:20px;">
    <div class="container" style="margin-bottom:10px;">
      <div class="col-md-4 col-sm-4">
        <select class="form-control "  name="month" id="month"  data-placeholder="Seleccione Mes">
          <option value=""></option>
          <option <?= $month_current == 1 ? 'selected' : '' ?> value="1">Enero</option>
          <option <?= $month_current == 2 ? 'selected' : '' ?> value="2">Febrero</option>
          <option <?= $month_current == 3 ? 'selected' : '' ?> value="3">Marzo</option>
          <option <?= $month_current == 4 ? 'selected' : '' ?> value="4">Abril</option>
          <option <?= $month_current == 5 ? 'selected' : '' ?> value="5">Mayo</option>
          <option <?= $month_current == 6 ? 'selected' : '' ?> value="6">Junio</option>
          <option <?= $month_current == 7 ? 'selected' : '' ?> value="7">Julio</option>
          <option <?= $month_current == 8 ? 'selected' : '' ?> value="8">Agosto</option>
          <option <?= $month_current == 9 ? 'selected' : '' ?> value="9">Septiembre</option>
          <option <?= $month_current == 10 ? 'selected' : '' ?> value="10">Octubre</option>
          <option <?= $month_current == 11 ? 'selected' : '' ?> value="11">Noviembre</option>
          <option <?= $month_current == 12 ? 'selected' : '' ?> value="12">Diciembre</option>
        </select>
      </div>
      <div class="col-md-4 col-sm-4">
        <select class="form-control " name="year" id="year" data-placeholder="Seleccione Año">
          <option value=""></option>
          <?php
            foreach ($years AS $year) {
              if($year['year'] == $year_current) {
                echo '<option selected >'.$year['year'].'</option>';
              } else {
                echo '<option>'.$year['year'].'</option>';
              }

            }
          ?>
        </select>
      </div>
      <div class="col-md-4 col-sm-4">
        <button type="submit" class="btn btn-primary" name="button"> <i class="fa fa-filter"></i> Filtrar </button>
      </div>
    </div>
    <div class="row" style="margin:15px;">
      <div class="col-md-6">
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3 style="font-size:60px;"><?= $orders[0]['cuantos'] ?></h3>
            <p style="font-size:18px;"> # Ordenes </p>
          </div>
          <div class="icon">
            <i class="fa fa-shopping-cart"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="small-box bg-red">
          <div class="inner">
            <h3 style="font-size:60px;"><?= $products ?></h3>
            <p style="font-size:18px;"> Total productos </p>
          </div>
          <div class="icon">
            <i class="fa fa-shopping-bag"></i>
          </div>
        </div>
      </div>
    </div>
    <div class="row" style="margin:15px;">
      <div class="col-md-6">
        <div class="small-box bg-yellow">
          <div class="inner">
            <h3 style="font-size:60px;"><?= $users[0]['cuantos'] ?></h3>
            <p style="font-size:18px;"> Usuarios registrados </p>
          </div>
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="small-box bg-green">
          <div class="inner">
            <h3 style="font-size:60px;"><?= $userActive[0]['cuantos']; ?></h3>
            <p style="font-size:18px;"> Usuarios activos </p>
          </div>
          <div class="icon">
            <i class="fa fa-users"></i>
          </div>
        </div>
      </div>
    </div>
  </section>
</form>
