<?php
$this->assign('title', __('Productos'));
$this->assign('section', __('Productos'));
?>
<section class="content-header">
	<h1>
		<?= $this->fetch('section') ?>
		<small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de productos') ?></h3>
			<br>
			<br>
			<a href="<?= $this->Url->build(['action' => 'add_product']) ?>" class="btn btn-default btn-sm product_add_product" title="Agregar" style="float: right;">
				<i class="fa fa-plus"></i> <?= __('Agregar') ?>
			</a>
			<a  onclick="open_cargar()" style="float: right;margin-right:5px;" class="btn btn-sm product_upload_product  btn-danger">
				<i class="fa fa-upload"></i> <?= __('Cargar productos') ?>
			</a>
			<a  onclick="open_cargar_cross()" style="float: right;margin-right:5px;" class="btn btn-sm product_upload_cross_references  btn-danger">
				<i class="fa fa-upload"></i> <?= __('Cargar cross references') ?>
			</a>
			<a  href="<?= $this->Url->build('/files/example/list_products.csv') ?>" style="float: right;margin-right:5px;" class="btn btn-sm product_download_format  btn-default">
				<i class="fa fa-download"></i> <?= __('Descargar formato productos') ?>
			</a>
			<a href="<?= $this->Url->build('/files/example/cross_references.csv') ?>" style="float: right;margin-right:5px;" class="btn btn-sm cross_download_cross  btn-default">
				<i class="fa fa-download"></i> <?= __('Descargar formato cross references') ?>
			</a>
			<a href="<?= $this->Url->build('/files/example/huerfanas.csv') ?>" style="float: right;margin-right:5px;" class="btn btn-sm  btn-default">
				<i class="fa fa-download"></i> <?= __('Descargar huerfanos') ?>
			</a>
		</div>
		<?php
		if (isset($file)):
			foreach ($errores as $e) {
				$error = $e['cuantos'];
			}

		?>
			<?php foreach ($file as $r): ?>
				<hr>
				<div class="box-body">
					<h3 class="text-primary"> <i class="fa fa-info-circle"></i> Resumen general </h3>
					<div class="col-sm-4">
						<div class="small-box bg-yellow">
							<div class="inner">
								<h3><?= number_format($r->records) ?></h3>
								<p>Registros en archivo </p>
							</div>
							<div class="icon">
								<i class="fa fa-list-alt"></i>
							</div>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="small-box bg-green">
							<div class="inner">
								<h3><?= number_format($r->records - $error) ?></h3>
								<p> Registros actualizados </p>
							</div>
							<div class="icon">
								<i class="fa fa-check"></i>
							</div>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="small-box bg-red">
							<div class="inner">
								<h3><?= number_format($error) ?> </h3>
								<p> Errores </p>
							</div>
							<div class="icon">
								<i class="fa fa-times"></i>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<?php $i=0; if($error > 0): ?>
					<div class="box-body">
						<h3 class="text-danger"> <i class="fa fa-times-circle"></i> Resumen errores </h3>
						<table class="table table-bordered table-stripped table-hover">
							<thead>
								<tr style="font-weight: bold;">
									<td width="5%">Item</td>
									<td width="95%">Detalle</td>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($reports AS $detalle):  $i++; ?>
									<tr>
										<td><?= $i; ?></td>
										<td><?= $detalle['motivo'] ?> ( <b class="text-danger"><?= $detalle['cuantos'] ?></b> )</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<hr>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
		<div class="box-body">
			<?php if( $products->count() == 0 ): ?>
				<div class="alert alert-info">
					<?= __('No hay productos para mostrar.') ?>
				</div>
			<?php else: ?>
				<table class="table dataTables table-bordered table-condensed table-striped table-hover">
					<thead>
						<tr>
							<th width="10%"><?= __('Código') ?></th>
							<th width="30%"><?= __('Tipo producto') ?></th>
							<th width="10%"><?= __('Marca') ?></th>
							<th width="10%"><?= __('Linea') ?></th>
							<th width="10%"><?= __('Status') ?></th>
							<th width="15%" style="text-align: center;"><?= __('Fecha creacion') ?></th>
							<th width="10%">&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($products as $product): ?>
							<tr>
								<td>
									<?= h($product->code) ?>
								</td>
								<td>
									<p data-toggle="tooltip" title="<?= h($product->type_product .' - '. $product->models) ?>">
									<?= h(substr($product->type_product .' - '. $product->models,0,50)) .' ...' ?>
									</p>
								</td>
								<td>
									<?= h($product->tbl_family->tbl_brand['name']) ?>
								</td>
								<td>
									<?= h($product->tbl_family->name) ?>
								</td>
								<td>
									<?php
									switch ($product->status) {
										case 'N':
											echo __('NUEVO');
											break;
										case 'D':
											echo __('DESCUENTO');
											break;
										case 'L':
											echo __('LIQUIDACION');
											break;
										case 'NA':
											echo __('NO APLICA');
											break;
										case 'PM':
											echo __('PRECIO MEJORADO');
											break;
									}
									?>
								</td>
								<td align="center">
									<?= h($product->created->format('m/d/y h:i')) ?>
								</td>
								<td class="text-right">
									<a href="<?= $this->Url->build(['action' => 'edit_product', $product->code]) ?>" class="btn btn-warning btn-sm product_edit_product">
										<i class="fa fa-edit"></i>
									</a>
									<a href="<?= $this->Url->build(['action' => 'delete_product', $product->code]) ?>" class="btn btn-danger btn-sm product_delete_product"
										onclick="return confirm('<?= __("¿Esta seguro que desea eliminar $product->type_product ")  ?>?');">
										<i class="fa fa-trash-o"></i>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	</div>
</section>

<!-- Modals -->
<form autocomplete="off" action="<?= $this->Url->build(['action' => 'upload_product']) ?>" enctype="multipart/form-data" method="post">
	<div id="cargue_product" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title"><i class="fa fa-upload"></i> <?= __("Cargue productos ")  ?> </h4>
	      </div>
	      <div class="modal-body">
					<p class="text-info align_center"><i class="fa fa-info-circle"></i> <?= __(" Antes de subir el archivo por favor verifique que la linea de cada producto esten creadas")  ?> </p>
					<br>
	        <label class="control-label col-md-2"> <?= __("Seleccione archivo: ")  ?>  </label>
					<div class="col-md-6">
						<input type="file" name="file" class="form-control">
					</div>
					<div class="clearfix">
					</div>
	      </div>
	      <div class="modal-footer">
					<button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> <?= __("Cargar productos")  ?> </button>
	        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("Cancelar")  ?></button>
	      </div>
	    </div>
	  </div>
	</div>
</form>

<form autocomplete="off" action="<?= $this->Url->build(['action' => 'upload_cross_refereces']) ?>" enctype="multipart/form-data" method="post">
	<div id="cargue_cross" class="modal fade" role="dialog">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title"><i class="fa fa-upload"></i> <?= __("Cargue cross references")  ?> </h4>
	      </div>
	      <div class="modal-body">
					<p class="text-info align_center"><i class="fa fa-info-circle"></i> <?= __(" Antes de subir el archivo por favor verifique que cada producto este creado ")  ?> </p>
					<br>
	        <label class="control-label col-md-2"> <?= __("Seleccione archivo: ")  ?>  </label>
					<div class="col-md-6">
						<input type="file" name="file_cross" class="form-control">
					</div>
					<div class="clearfix">
					</div>
	      </div>
	      <div class="modal-footer">
					<button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> <?= __("Cargar cross references")  ?> </button>
	        <button type="button" class="btn btn-default" data-dismiss="modal"><?= __("Cancelar")  ?></button>
	      </div>
	    </div>
	  </div>
	</div>
</form>

<?php $this->append('scripts'); ?>
<script type="text/javascript">

function open_cargar () {
  $('#cargue_product').modal('show')
}

function open_cargar_cross () {
  $('#cargue_cross').modal('show')
}
</script>
<?php $this->end(); ?>
