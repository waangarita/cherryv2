<?php
	$this->assign('title', 'Roles');
	$this->assign('section', 'Roles');
?>
<section class="content-header">
	<h1>
        <?= $this->fetch('section') ?>
        <small>Listado</small>
	</h1>
	<ol class="breadcrumb">
        <li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
        <div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de Roles') ?></h3>
			<a href="<?= $this->Url->build(['action' => 'add_role']) ?>" class="btn btn-default btn-sm setup_add_role" title="Agregar" style="float: right;">
				<i class="fa fa-plus"></i>
			</a>
        </div>
        <div class="box-body">
			<?php if( $roles->count() == 0 ): ?>
			<div class="alert alert-info">
				<?= __('No hay roles para mostrar.') ?>
			</div>
			<?php else: ?>
			<table class="table table-condensed table-striped table-hover">
				<tr>
					<th><?php echo $this->Paginator->sort('name', __('Rol')); ?></th>
					<th>&nbsp;</th>
				</tr>
				<?php foreach($roles as $role): ?>
				<tr>
					<td>
						<?= h($role->name) ?>
					</td>
					<td class="text-right">
						<a href="<?= $this->Url->build(['action' => 'edit_role', $role->id]) ?>" class="btn btn-warning btn-sm setup_edit_role" title="Ver">
							<i class="fa fa-edit"></i>
						</a>
						<a href="<?= $this->Url->build(['action' => 'delete_role', $role->id]) ?>" class="btn btn-danger btn-sm setup_delete_role"
						   data-confirm="¿<?= __('Esta seguro que desea eliminar')?> '<?= $role->name ?>'?">
							<i class="fa fa-trash-o"></i>
						</a>
					</td>
				</tr>
				<?php endforeach; ?>
			</table>
			<ul class="pagination">
				<?= $this->Paginator->first('«') ?>
				<?= $this->Paginator->numbers() ?>
				<?= $this->Paginator->last('»') ?>
			</ul>
			<?php endif; ?>
        </div>
	</div>
</section>