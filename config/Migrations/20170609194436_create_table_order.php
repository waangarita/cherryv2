<?php

use Phinx\Migration\AbstractMigration;

class CreateTableOrder extends AbstractMigration
{
  /**
  * Change Method.
  *
  * Write your reversible migrations using this method.
  *
  * More information on writing migrations is available here:
  * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
  *
  * The following commands can be used in this method and Phinx will
  * automatically reverse them when rolling back:
  *
  *    createTable
  *    renameTable
  *    addColumn
  *    renameColumn
  *    addIndex
  *    addForeignKey
  *
  * Remember to call "create()" or "update()" and NOT "save()" when working
  * with the Table class.
  */
  public function change()
  {
    $table = $this->table('tbl_order');
    $table  ->  addColumn('id_user', 'integer')
            ->  addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP', 'null' => 'true' ))
            ->  create();

    $refTable = $this->table('tbl_order');
    $refTable ->  addForeignKey('id_user', 'adm_user', 'id', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
              ->  update();

    $table_detail = $this->table('tbl_detail_order');
    $table_detail ->  addColumn('id_order', 'integer')
                  ->  addColumn('id_product', 'string')
                  ->  addColumn('amount', 'decimal')
                  ->  addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP', 'null' => 'true' ))
                  ->  addColumn('modified', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
                  -> create();

    $refTableDetail = $this->table('tbl_detail_order');
    $refTableDetail ->  addForeignKey('id_order', 'tbl_order', 'id', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
                    ->  addForeignKey('id_product', 'tbl_product', 'code', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
                    ->  update();
  }
}
