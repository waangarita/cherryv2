<?php

use Phinx\Migration\AbstractMigration;

class CreateTableProducts extends AbstractMigration
{
    /**
    * Change Method.
    *
    * Write your reversible migrations using this method.
    *
    * More information on writing migrations is available here:
    * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
    *
    * The following commands can be used in this method and Phinx will
    * automatically reverse them when rolling back:
    *
    *    createTable
    *    renameTable
    *    addColumn
    *    renameColumn
    *    addIndex
    *    addForeignKey
    *
    * Remember to call "create()" or "update()" and NOT "save()" when working
    * with the Table class.
    */
    public function change()
    {
        $table = $this->table('tbl_product', ['id' => false, 'primary_key' => 'code']);
        $table  ->  addColumn('code', 'string')
                ->  addColumn('id_family', 'string')
                ->  addColumn('type_product', 'string')
                ->  addColumn('product_series', 'text')
                ->  addColumn('models', 'text')
                ->  addColumn('presentation', 'string')
                ->  addColumn('per_master', 'integer')
                ->  addColumn('weight', 'string')
                ->  addColumn('status', 'enum', ['values' => ['N', 'D', 'L', 'NA'] ] )
                ->  addColumn('active', 'boolean')
                ->  addColumn('price_suggested', 'integer')
                ->  addColumn('img', 'string')
                ->  addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP', 'null' => 'true' ))
                ->  addColumn('modified', 'timestamp', array('default' => 'CURRENT_TIMESTAMP'))
                ->  create();

        $refTable = $this->table('tbl_product');
        $refTable ->  addForeignKey('id_family', 'tbl_family', 'code', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
                    ->  update();
    }
}
