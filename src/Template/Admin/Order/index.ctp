<?php
$this->assign('title', __('Ordenes pedidos'));
$this->assign('section', __('Ordenes pedidos'));
?>
<section class="content-header">
	<h1>
		<?= $this->fetch('section') ?>
		<small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de ordenes pedidos') ?></h3>
		</div>
		<div class="box-body">
			<?php if( $orders->count() == 0 ): ?>
				<div class="alert alert-info">
					<?= __('No hay clientes para mostrar.') ?>
				</div>
			<?php else: ?>
				<table class="table dataTables table-condensed table-striped table-hover">
					<thead>
						<tr>
							<th><?= __('# Pedido') ?></th>
							<th><?= __('Productos') ?></th>
							<th><?= __('Status') ?></th>
							<th style="text-align: center;"><?= __('Fecha creacion') ?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach($orders AS $order):
							$detail = $detail_order->detailOrder($order->id_user,$order->id);
							?>
							<tr>
								<td>
									<?= h($order->id) ?>
								</td>
								<td>
									<ul style="list-style:none">
										<?php
										foreach ($detail AS $value) {
											?>
											<li><?= h($value['id_product']) ." - ". h($value['type_product']) ?></li>
											<?php
										}
										?>
									</ul>
								</td>
								<td>
									<?= h($order->status) ?>
								</td>
								<td align="center">
									<?= h($order->created->format('m/d/y h:i')) ?>
								</td>
								<td class="text-right">
									<a href="<?= $this->Url->build(['action' => 'view_order', $order->id]) ?>" class="btn btn-warning btn-sm client_edit_client">
										<i class="fa fa-edit"></i>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	</div>
</section>
