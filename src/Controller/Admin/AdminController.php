<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

class AdminController extends AppController
{
	public function initialize()
	{
		parent::initialize();

		// load models
		$this->loadModel('AdmRole');
		$this->loadModel('AdmMenu');
		$this->loadModel('AdmMenu2Role');
		$this->loadModel('AdmSection');

		// load components
		$this->loadComponent('Paginator');
		$this->loadComponent('Auth', [
			'loginAction' => ['controller' => 'Auth', 'action' => 'login'],
			'loginRedirect' => ['controller' => 'Setup', 'action' => 'dashboard'],
			'logoutRedirect' => ['controller' => 'Auth', 'action' => 'login'],
			'authenticate' => [
				'Form' => [
					'userModel' => 'AdmUser',
					'fields' => ['username' => 'email', 'password' => 'password']
				]
			],
			'storage' => 'Session'
		]);

		$this->viewBuilder()->layout('admin');

		// global vars
		$auth = $this->Auth->user();
		$this->set('auth', $auth);

		if (!empty($auth)) {
			$this->setSidebar();
			$this->setPrivileges($auth);
		}
	}

	private function setSidebar(){
		$menuItems = $this->AdmMenu->find()
								   ->select(['AdmMenu.id', 'AdmMenu.menu_id', 'AdmMenu.display_name', 'AdmMenu.icon', 'AdmSection.controller', 'AdmSection.action'])
								   ->contain(['AdmSection',
								   			  'AdmMenu2Role',
								   			  'AdmMenuChild' => function ($q) {
				   									// must have the section info and roles
				   									return $q->contain(['AdmSection','AdmMenu2Role'])
																		 ->order(['AdmMenuChild.position, AdmMenuChild.display_name ASC']);
								   			  }
								   ])
								   ->where(['AdmMenu.menu_id is null'])
								   ->order(['AdmMenu.position, AdmMenu.display_name ASC'])->toArray();

		$this->set('authMenuItems', $menuItems);
	}

	private function setPrivileges($auth){
		$privilegesCss = [];

		$sections = $this->AdmSection->findSectionsEnabledForRoleId($auth['role_id']);

		foreach ($sections as $section) {
			$sectionInfoArr = [];
			$sectionInfoArr[] = strtolower($section['controller']);
			$sectionInfoArr[] = strtolower($section['action']);
			$privilegesCss[] = '.' . implode('_', $sectionInfoArr) . '{display: none !important;}';
		}

		$this->set('privilegesCss', $privilegesCss);
	}

}
