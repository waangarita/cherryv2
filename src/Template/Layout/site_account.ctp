<!DOCTYPE html>
<html lang="en">
<head>
  <title> Cherry </title>
  <meta charset="utf-8">
  <meta name="description" content="cherry Products">
  <meta name="author" content="" />
  <meta name="keywords" content="global, cherry, products" />
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">

  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <!--Favicon-->
  <link rel="shortcut icon" href="<?= $this->Url->build('/img/favicon.ico') ?>" type="image/x-icon">
  <link rel="icon" href="<?= $this->Url->build('/img/favicon.ico') ?>" type="image/x-icon">

  <!-- css files -->
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/bootstrap.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/font-awesome.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/owl.carousel.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/owl.theme.default.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/animate.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/swiper.css') ?>" />

  <!-- this is default skin you can replace that with: dark.css, yellow.css, red.css ect -->
  <link id="pagestyle" rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/skin-blue.css') ?>" />

  <!-- Nuevos Estilos cherry -->
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/new_style.css') ?>" />

  <!-- Google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/css/sweetalert.css') ?>">
</head>
<body>

  <!-- start topBar -->
  <div class="topBar">
    <div class="container">
      <ul class="list-inline pull-left hidden-sm hidden-xs">
        <li><span class="text-primary"> <?= __('Tienes alguna pregunta') ?>? </span> <?= __('Llamanos') ?> +57 316 495 3275 </li>
      </ul>

      <ul class="topBarNav pull-right">
        <?php if ($language == 'ES'): ?>
        <li class="linkdown">
          <a href="javascript:void(0);">
            <img src="<?= $this->Url->build('/maqueta/img/flags/flag-spain.jpg') ?>" class="mr-5" alt="">
            <span class="hidden-xs">
              Español
              <i class="fa fa-angle-down ml-5"></i>
            </span>
          </a>
          <ul class="w-100">
            <li><a href="" onclick="language('EN',event)"><img src="<?= $this->Url->build('/maqueta/img/flags/flag-english.jpg') ?>" class="mr-5" alt=""> English </a></li>
          </ul>
        </li>
        <?php else: ?>
          <li class="linkdown">
            <a href="javascript:void(0);">
              <img src="<?= $this->Url->build('/maqueta/img/flags/flag-english.jpg') ?>" class="mr-5" alt="">
              <span class="hidden-xs">
                English
                <i class="fa fa-angle-down ml-5"></i>
              </span>
            </a>
            <ul class="w-100">
              <li>
                <a href="" onclick="language('ES',event)">
                  <img src="<?= $this->Url->build('/maqueta/img/flags/flag-spain.jpg') ?>" class="mr-5" alt=""> Español
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>
        <li class="linkdown">
          <a href="javascript:void(0);">
            <i class="fa fa-user mr-5"></i>
            <span class="hidden-xs">
              <?= __('Mi cuenta') ?>
              <i class="fa fa-angle-down ml-5"></i>
            </span>
          </a>
          <ul class="w-150">
            <li><a href="<?= $this->Url->build(['action' => 'my-account']) ?>"><?= __('Mi cuenta') ?></a></li>
            <li class="divider"></li>
            <li><a href="<?= $this->Url->build(['action' => 'logout']) ?>"><?= __('Cerrar Sesion') ?></a></li>
          </ul>
        </li>
        <?= $this->element('Partials/cart') ?>
        <li class="linkdown">
          <a href="<?= $this->Url->build(['action' => 'logout']) ?>"> <i class="fa fa-sign-out"></i> <?= __('Cerrar Sesion') ?></a>
        </li>
      </ul>

    </div><!-- end container -->
  </div>
  <!-- end topBar -->

  <!-- start navbar -->
  <div class="navbar yamm navbar-default">
    <div class="container">
      <div class="navbar-header">
        <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="navbar-brand">
          <img src="<?= $this->Url->build('/images/cherry-logo.png') ?>" width="200px" alt="logo">
        </a>
      </div>
      <div class="col-md-2"></div>
      <div  id="navbar-collapse-1" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a href="<?= $this->Url->build(['action' => 'index']) ?>"><?= __('Home') ?></a></li>

          <li><a href="<?= $this->Url->build(['action' => 'products']) ?>"><?= __('Productos') ?></a></li>

          <li><a href="<?= $this->Url->build(['action' => 'promotion']) ?>"><?= __('Promociones') ?> </a></li>

          <li><a href="<?= $this->Url->build(['action' => 'faq']) ?>"><?= __('FAQ') ?> </a></li>
        </ul>
      </div><!-- end navbar collapse -->
    </div><!-- end container -->
  </div><!-- end navbar -->

  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <ul>
            <li><a href="<?= $this->Url->build(['action' => 'index']) ?>"><?= __('Inicio') ?></a></li>
            <?= $breadcrumbs ?>
          </ul><!-- end breadcrumb -->
        </div><!-- end col -->
      </div><!-- end row -->
    </div><!-- end container -->
  </div><!-- end breadcrumbs -->

  <!-- start section -->
  <section class="section white-backgorund">
    <div class="container">
      <div class="row">
        <!-- start sidebar -->
        <div class="col-sm-3">
          <div class="widget">
            <h6 class="subtitle"><?= __('Navegacion mi cuenta') ?></h6>

            <ul class="list list-unstyled">
              <li class="active">
                <a href="<?= $this->Url->build(['action' => 'my-account']) ?>"><?= __('Mi cuenta') ?> </a>
              </li>
              <?php if ($auth['role_id'] <> 3): ?>
              <li>
                <a href="<?= $this->Url->build(['action' => 'cart']) ?>"> <?= __('Pedido') ?> </span></a>
              </li>
              <li>
                <a href="<?= $this->Url->build(['action' => 'historyOrders']) ?>"><?= __('Historial Ordenes') ?></a>
              </li>
              <li>
                <a href="<?= $this->Url->build(['action' => 'getListProducts']) ?>"><?= __('Descargar Lista Precios') ?></a>
              </li>
              <?php endif; ?>
            </ul>
          </div><!-- end widget -->
        </div><!-- end col -->
        <!-- end sidebar -->
        <?= $this->fetch('content') ?>
      </div><!-- end row -->
    </div><!-- end container -->
  </section>
  <!-- end section -->

  <!-- BANNER INFERIOR Y FOOTER -->
  <?= $this->element('Partials/footer') ?>
  <!-- END BANNER INFERIOR Y FOOTER -->

  <!-- LENGUAGE -->
  <script type="text/javascript">
    const LANGUAGE = '<?= $language  ?>'
    const API = '<?= $this->Url->build("/api/") ?>'
    const URL_IMAGES = '<?= $this->Url->build('/images/') ?>'
    const ROOT = '<?= $this->Url->build('/') ?>'
  </script>
  <!-- END LENGUAGE -->

  <!-- JavaScript Files -->
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/jquery-3.1.1.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/bootstrap.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/owl.carousel.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/jquery.downCount.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/nouislider.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/jquery.sticky.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/pace.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/star-rating.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/wow.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/swiper.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/main.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/js/translate.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/js/general.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/js/sweetalert.min.js') ?>"></script>
  <script>
    $(document).ready(function () {
      getCartByUser (<?= $auth['id'] ?>)

      $('#update-information').hide()

      $('#show-information').click( function () {
        $('#update-information').show()
        $('#information-user').hide()
      })

      $('#update-information').submit(()=> {
        if ($('#password').val() !== $('#password2').val() ) {
          $('#message').html('<div  class="alert alert-danger"> <i class="fa fa-times-circle" style="font-size:15pt;"></i> El password no coincide por favor validar</div>')
          $('#password').focus()
          return false;
        }else{
          $('#message').html('')
        }
      })
    })
  </script>
  <?= $this->fetch('scripts') ?>
</body>
</html>
