<?php
use Cake\Routing\Router;
$path = Router::url('/', true);
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<body>
  <div>
    <img src="http://cherryext.miro.beecloud.me/images/cherry-logo.png" width="300px" alt="">
  </div>
  <hr>
  <div>
    <h3>Hola <?= $info['nombres'] .' '.$info['apellidos'];  ?>! </h3>
    <p>Hemos registrado un cambio en tu password, nueva password <b><?= $info['password'] ?></b>  </p>
    <p>Muchas Gracias.</p>
  </div>
</body>
