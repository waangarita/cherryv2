<?php
$this->assign('title', __('Listas precios'));
$this->assign('section', __('Listas precios'));
?>
<section class="content-header">
	<h1>
		<?= $this->fetch('section') ?>
		<small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de listas precios') ?></h3>
			<a  onclick="open_cargar()" style="float: right;" class="btn btn-sm btn-danger listprice_upload_listprice">
				<i class="fa fa-upload"></i> <?= __('Cargar listas') ?>
			</a>
			<a  href="<?= $this->Url->Build('/files/example/price_list.csv') ?>" style="float: right;margin-right:5px;" class="btn btn-sm listprice_download_format  btn-info">
				<i class="fa fa-download"></i> <?= __('Descargar formato') ?>
			</a>
		</div>
		<div class="box-body">
			<form autocomplete="off" action="<?= $this->Url->build(['action' => 'addListPrice']) ?>" method="post">
				<div class="form-group has-feedback">
					<label><?= __('Nombre lista precio') ?> <span class="text-danger">*</span></label>
					<input type="text" required="true" onKeyPress="javascript:return no_caracteres ( event )" name="name" class="form-control" placeholder="<?= __('Nombre lista precio') ?>">
				</div>
				<div class="row">
					<div class="col-xs-4">
					</div>
					<div class="col-xs-offset-4 col-xs-4 text-right">
						<button type="submit" class="btn btn-primary ListPrice_add_ListPrice"> <i class="fa fa-save"></i> <?= __('Crear') ?></button>
					</div>
				</div>
			</form>
		</div>
		<hr>
		<?php
		if (isset($report)):
			$errores = $count[0]['cuantos'];
		?>
			<?php foreach ($report as $r): ?>
				<div class="box-body">
					<h3 class="text-primary"> <i class="fa fa-info-circle"></i> Resumen general </h3>
					<div class="col-sm-4">
						<div class="small-box bg-yellow">
							<div class="inner">
								<h3><?= number_format($r->records) ?></h3>
								<p>Registros en archivo </p>
							</div>
							<div class="icon">
								<i class="fa fa-list-alt"></i>
							</div>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="small-box bg-green">
							<div class="inner">
								<h3><?= number_format($r->records - $errores) ?></h3>
								<p> Registros actualizados </p>
							</div>
							<div class="icon">
								<i class="fa fa-check"></i>
							</div>
						</div>
					</div>

					<div class="col-sm-4">
						<div class="small-box bg-red">
							<div class="inner">
								<h3><?= number_format($errores) ?> </h3>
								<p> Errores </p>
							</div>
							<div class="icon">
								<i class="fa fa-times"></i>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<?php $i=0; if($errores > 0): ?>
					<div class="box-body">
						<h3 class="text-danger"> <i class="fa fa-times-circle"></i> Resumen errores </h3>
						<table class="table table-bordered table-stripped table-hover">
							<thead>
								<tr style="font-weight: bold;">
									<td width="5%">Item error</td>
									<td width="95%">Detalle</td>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($r->Resumen AS $detalle):  $i++; ?>
									<tr>
										<td>Error <?= $i; ?></td>
										<td><?= $detalle['motivo'] ?> ( <b class="text-danger"><?= $detalle['cuantos'] ?></b> )</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<hr>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>




		<div class="box-body">
			<?php if( $list_price->count() == 0 ): ?>
				<div class="alert alert-info">
					<?= __('No hay listas de precios para mostrar.') ?>
				</div>
			<?php else: ?>
				<table class="table dataTables table-bordered table-striped">
					<thead>
						<tr>
							<th><?= __('Nombre') ?></th>
							<th><?= __('Cantidad productos') ?></th>
							<th style="text-align: center;"><?= __('Fecha creacion') ?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($list_price as $list): ?>
							<tr>
								<td>
									<?= h($list->name) ?>
								</td>
								<td>
									<?= h(count($list['detailList'])) ?>
								</td>
								<td align="center">
									<?= h($list->created->format('m/d/y h:i')) ?>
								</td>
								<td class="text-right">
									<a href="<?= $this->Url->build(['action' => 'view_list_price', $list->name]) ?>" class="btn btn-default btn-sm ListPrice_view_ListPrice">
										<i class="fa fa-eye"></i>
									</a>
									<?php if($list->name !== 'Standard:'): ?>
										<a href="<?= $this->Url->build(['action' => 'delete_list_price', $list->name]) ?>" class="btn btn-danger btn-sm TpListPrice_delete_TpListPrice"
											onclick="return confirm('<?= __("Si usted elimina esta lista de precios se eliminara todos los clientes y usuarios asociados a ella , ¿Esta seguro que desea eliminar $list->name ")  ?>?');">
											<i class="fa fa-trash-o"></i>
										</a>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	</div>
</section>

<!-- Modal -->
<form autocomplete="off" action="<?= $this->Url->build(['action' => 'uploadList']) ?>" enctype="multipart/form-data" method="post">
	<div id="cargue" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><i class="fa fa-upload"></i> Cargue listas de precios</h4>
				</div>
				<div class="modal-body">
					<label class="control-label col-md-2"> Seleccione archivo: </label>
					<div class="col-md-6">
						<input type="file" name="file_list" class="form-control">
					</div>
					<div class="clearfix">

					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Cargar</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				</div>
			</div>
		</div>
	</div>
</form>

<?php $this->append('scripts'); ?>
<script type="text/javascript">

function open_cargar() {
	$('#cargue').modal('show')
}
</script>
<?php $this->end(); ?>
