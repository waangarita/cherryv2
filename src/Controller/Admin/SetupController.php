<?php

namespace App\Controller\Admin;

class SetupController extends AdminController {

	public function initialize()
	{
		parent::initialize();

		// load models
		$this->loadModel('AdmUser');
		$this->loadModel('AdmPrivilege');
		$this->loadModel('TblClient');
		$this->loadModel('TblCountry');
		$this->loadModel('TblLogLogin');
		$this->loadModel('TblOrder');
		$this->loadModel('TblProduct');
	}

	public function dashboard()
	{
		$years = $this->TblLogLogin->getYearsOperation();
		$products = $this->TblProduct->find()->where(['active' => 1])->count();

		if ($this->request->is('post')) {
			$month_current = $this->request->data['month'];
			$year_current = $this->request->data['year'];
			$orders = $this->TblOrder->countOrders($month_current, $year_current);
			$userActive = $this->TblLogLogin->getUserActive($month_current, $year_current);
			$users = $this->AdmUser->getUserRegister($month_current, $year_current);
		} else {
			$month_current = date('m');
			$year_current = date('Y');
			$orders = $this->TblOrder->countOrders($month_current, $year_current);
			$userActive = $this->TblLogLogin->getUserActive($month_current, $year_current);
			$users = $this->AdmUser->getUserRegister($month_current, $year_current);
		}

		$this->set(compact('orders'));
		$this->set(compact('users'));
		$this->set(compact('products'));
		$this->set(compact('years'));
		$this->set(compact('year_current'));
		$this->set(compact('month_current'));
		$this->set(compact('userActive'));
	}

	/*
	* Users management
	*/
	public function indexUser(){
		$loguser = $this->Auth->user();
		$users = $this->AdmUser->findRegularUsers();
		$this->set('users', $users);
	}

	public function addUser () {
		$countries = $this->TblCountry->find();
		$roles = $this->AdmRole->findRegularRoles();
		$clients = $this->TblClient->find()->where(['active' => 1]);
		$this->set('roles', $roles);
		$this->set('clients', $clients);
		$this->set(compact('countries'));

		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user
			$user = $this->AdmUser->newEntity($this->request->data());
			$user->last_login = date('Y-m-d h:d:s');
			$user->country_id = $this->request->data['country_id'];

			if( $this->AdmUser->save($user) ) {
				$successMsg = sprintf("Usuario '%s %s' creado.", $user->first_name, $user->last_name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);

				$this->log($user,'debug');

				$this->log($this->request->data(),'debug');

				$from = 'no-reply@cherrytech.com';
				$to = $this->request->data['email'];
				$subject = 'Bienvenido '. $this->request->data['first_name'] .' '.$this->request->data['last_name'] .' a cherry' ;
				$template = 'welcome';

				$info['info'] = array(
											'password' => $this->request->data['password'],
											'nombres' => $this->request->data['first_name'],
											'apellidos' => $this->request->data['last_name']
										);

				$this->sendEmail($from, $to, $subject, $template, $info);
			}
			else{
				$failMsg = 'Ha ocurrido un error creando el usuario.';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($user->errors(), "error");
				$this->Flash->error($failMsg);
			}

			return $this->redirect(['action' => 'indexUser']);
		}
	}

	public function editUser ($id) {
		$countries = $this->TblCountry->find();
		$user = $this->AdmUser->get($id);
		$clients = $this->TblClient->find()->where(['active' => 1]);
		$this->set('user', $user);
		$this->set('clients', $clients);
		$this->set(compact('countries'));

		$roles = $this->AdmRole->findRegularRoles();
		$this->set('roles', $roles);

		if( $this->request->is('post') ) {
			if ($user->status == 'STANDBY' && $this->request->data['status'] == 'ACTIVE') {
				$sendWelcome = 1;
			}else{
				$sendWelcome = 0;
			}

			$loguser = $this->Auth->user(); //logged user
			$user->first_name = $this->request->data['first_name'];
			$user->last_name = $this->request->data['last_name'];
			$user->email = $this->request->data['email'];
			$user->phone1 = $this->request->data['phone1'];
			$user->phone2 = $this->request->data['phone2'];
			$user->id_client = $this->request->data['id_client'];
			$user->appoinment = $this->request->data['appoinment'];
			$user->role_id = $this->request->data['role_id'];
			$user->status = $this->request->data['status'];
			$user->country_id = $this->request->data['country_id'];
			if(!empty($this->request->data['password'])) {
				$user->password = $this->request->data['password'];
			}

			if( $this->AdmUser->save($user)) {
				$successMsg = sprintf("Usuario '%s %s' editado.", $user->first_name, $user->last_name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
				$this->log($user,'debug');
				$this->log($this->request->data(),'debug');

				if($sendWelcome == 1) {
					$from = 'no-reply@cherry.com';
					$to = $this->request->data['email'];
					$subject = 'Bienvenido '. $this->request->data['first_name'] .' '.$this->request->data['last_name'] .' a cherry ' ;
					$template = 'welcome';

					$info['info'] = array(
												'password' => 'la antes registrada',
												'nombres' => $this->request->data['first_name'],
												'apellidos' => $this->request->data['last_name']
											);

					$this->sendEmail($from, $to, $subject, $template, $info);
				}

				if ( !empty($this->request->data['password']) ) {
					$from = 'no-reply@cherry.com';
					$to = $this->request->data['email'];
					$subject = 'Cambio contraseña cherry';
					$template = 'change_password';

					$info['info'] = array(
												'password' => $this->request->data['password'],
												'nombres' => $this->request->data['first_name'],
												'apellidos' => $this->request->data['last_name']
											);

					$this->sendEmail($from, $to, $subject, $template, $info);
				}
			}
			else{
				$failMsg = 'Ha ocurrido un error editando el usuario.';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($user->errors(), "error");
				$this->Flash->error($failMsg);
			}

			return $this->redirect(['action' => 'indexUser']);
		}
	}

	public function deleteUser ($id){
		$user = $this->AdmUser->get($id);
		$user->status = 'DELETED';
		$loguser = $this->Auth->user(); //logged user
		if( $this->AdmUser->save($user) ) {
			$successMsg = sprintf("Usuario eliminado.");
			$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
			$this->Flash->success($successMsg);
		}
		else {
			$failMsg = 'Ha ocurrido un error eliminando al Usuario.';
			$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
			$this->log($user->errors(), "error");
			$this->Flash->error($failMsg);
		}
		return $this->redirect(['action' => 'indexUser']);
	}

	public function viewUser ($id){
		$user = $this->AdmUser->get($id);
		$this->set('user', $user);
	}

	/*
	* Roles management
	*/
	public function indexRole(){
		$query = $this->AdmRole->findRegularRoles();
		$roles = $this->Paginator->paginate($query, ['limit' => 25]);
		$this->set('roles', $roles);
	}

	public function addRole(){
		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			$role = $this->AdmRole->newEntity( $this->request->data() );
			if( $this->AdmRole->save($role) ) {
				$successMsg = sprintf("Rol '%s' creado.", $role->name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
			}
			else{
				$failMsg = 'Ha ocurrido un error creando el rol.';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($role->errors(), "error");
				$this->Flash->error($failMsg);
			}
			return $this->redirect(['action' => 'indexRole']);
		}
	}

	public function editRole($id){
		$role = $this->AdmRole->get($id);
		$this->set('role', $role);

		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			$role = $this->AdmRole->patchEntity($role, $this->request->data() );
			if( $this->AdmRole->save($role) ) {
				$successMsg = sprintf("Rol '%s' editado.", $role->name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
			}
			else{
				$failMsg = 'Ha ocurrido un error editado el rol.';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($role->errors(), "error");
				$this->Flash->error($failMsg);
			}
			return $this->redirect(['action' => 'indexRole']);
		}
	}

	public function deleteRole($id){
		$role = $this->AdmRole->get($id);
		$loguser = $this->Auth->user(); //logged user
		try{
			if( $this->AdmRole->delete($role) ) {
				$successMsg = sprintf("Rol eliminado. [%s]", $role->name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
				return $this->redirect(['action' => 'indexRole']);
			}
		}catch(\Exception $e){
			$failMsg = 'Verifique si el rol tiene secciones o usuarios asociados.';
			$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
			$this->Flash->error($failMsg);
			return $this->redirect(['action' => 'indexRole']);
		}
	}

	/*
	* Sections management
	*/
	public function indexSection(){
		$query = $this->AdmSection->find()->order(['name ASC']);
		$sections = $this->Paginator->paginate($query, ['limit' => 50]);
		$this->set('sections', $sections);
	}

	public function addSection(){
		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			$section = $this->AdmSection->newEntity( $this->request->data() );
			if( $this->AdmSection->save($section) ) {
				$successMsg = sprintf("Sección '%s' creada.", $section->name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
			}
			else{
				$failMsg = 'Ha ocurrido un error creando la sección.';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($section->errors(), "error");
				$this->Flash->error($failMsg);
			}
			return $this->redirect(['action' => 'indexSection']);
		}
	}

	public function editSection($id){
		$section = $this->AdmSection->get($id);
		$this->set('section', $section);

		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			$section = $this->AdmSection->patchEntity($section, $this->request->data() );
			if( $this->AdmSection->save($section) ) {
				$successMsg = sprintf("Sección '%s' editada.", $section->name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
			}
			else{
				$failMsg = 'Ha ocurrido un error editando la sección.';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($section->errors(), "error");
				$this->Flash->error($failMsg);
			}
			return $this->redirect(['action' => 'indexSection']);
		}
	}

	public function deleteSection($id){
		$section = $this->AdmSection->get($id);
		$loguser = $this->Auth->user(); //logged user
		if( $this->AdmSection->delete($section) ) {
			$successMsg = sprintf("Sección eliminada.");
			$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
			$this->Flash->success($successMsg);
		}
		else {
			$failMsg = 'Ha ocurrido un error eliminando la sección.';
			$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
			$this->log($section->errors(), "error");
			$this->Flash->error($failMsg);
		}
		return $this->redirect(['action' => 'indexSection']);
	}

	/*
	* Privileges management
	*/
	public function indexPrivilege(){
		$roles = $this->AdmRole->findRegularRoles();
		$this->set('roles', $this->Paginator->paginate($roles, ['limit' => 100]));
	}

	public function configPrivilege($id){
		$sections = $this->AdmSection->find()->select(['id', 'name'])->order(['id ASC']);
		$privileges = $this->AdmPrivilege->find()
		->where(['role_id' => $id])
		->order(['section_id ASC'])->toArray();

		$this->set('sections', $this->Paginator->paginate($sections, ['limit' => 100	]));
		$this->set('privileges', $privileges);
		$this->set('rol', $this->AdmRole->get($id));

		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			//clear all privileges
			$this->AdmPrivilege->deleteAllByRoleId($id);

			foreach ($sections as $section) {
				if(!empty($this->request->data[$section->id . '_privilege'])){
					$privilege = $this->AdmPrivilege->newEntity();
					$privilege->role_id = $id;
					$privilege->section_id = $section->id;
					$this->AdmPrivilege->save($privilege);
				}
			}
			$successMsg = "Configuración realizada.";
			$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
			$this->Flash->success($successMsg);
			return $this->redirect(['action' => 'indexPrivilege']);
		}
	}

	/*
	* Menu management
	*/
	public function indexMenu(){
		$this->set('menuItems', $this->getMenuItems());
	}

	public function addMenu(){
		$this->set('menuItems', $this->getMenuItems());
		$this->set('sections', $this->AdmSection->find()->select(['id', 'name'])->order(['name ASC']));
		$this->set('roles', $this->AdmRole->findRegularRoles());
		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			$menuItem = $this->AdmMenu->newEntity( $this->request->data() );
			if( $this->AdmMenu->save($menuItem) ) {
				if(!empty($this->request->data['role_id'])){
					$roleIds = $this->request->data['role_id'];

					//save all the selected roles
					foreach ($roleIds as $roleId) {
						$role = $this->AdmRole->get($roleId);
						$menu2role = $this->AdmMenu2Role->newEntity();
						$menu2role->menu_id = $menuItem->id;
						$menu2role->role_id = $role->id;
						if($this->AdmMenu2Role->save($menu2role)){
							$successMsg = sprintf("Item de menú '%s' visible para rol '%s'.", $menuItem->display_name, $role->name);
							$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
						}
						else{
							$failMsg = sprintf("Ha ocurrido un error creando la asociacion del Item de menú '%s' y el rol '%s'.", $menuItem->display_name, $role->name);
							$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
						}
					}
				}

				$successMsg = sprintf("Item de menú '%s' creado.", $menuItem->display_name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
			}
			else{
				$failMsg = 'Ha ocurrido un error creando la sección.';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($menuItem->errors(), "error");
				$this->Flash->error($failMsg);
			}
			return $this->redirect(['action' => 'indexMenu']);
		}
	}

	public function editMenu($id){
		$menuItem = $this->AdmMenu->get($id, ['contain' => ['AdmMenuParent', 'AdmMenu2Role.AdmRole' => function ($q) {return $q->select(['id','name']);}]]);

		$this->set('menuItem', $menuItem);
		$this->set('menuItems', $this->getMenuItems());
		$this->set('sections', $this->AdmSection->find()->select(['id', 'name'])->order(['name ASC']));
		$this->set('roles', $this->AdmRole->findRegularRoles());

		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			$menuItem = $this->AdmMenu->patchEntity( $menuItem, $this->request->data() );
			if( $this->AdmMenu->save($menuItem) ) {
				if(!empty($this->request->data['role_id'])){
					$roleIds = $this->request->data['role_id'];

					//delete all the associated roles
					$this->AdmMenu2Role->deleteAllByMenuId($menuItem->id);

					//save all the selected roles
					foreach ($roleIds as $roleId) {
						$role = $this->AdmRole->get($roleId);
						$menu2role = $this->AdmMenu2Role->newEntity();
						$menu2role->menu_id = $menuItem->id;
						$menu2role->role_id = $role->id;
						if($this->AdmMenu2Role->save($menu2role)){
							$successMsg = sprintf("Item de menú '%s' visible para rol '%s'.", $menuItem->display_name, $role->name);
							$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
						}
						else{
							$failMsg = sprintf("Ha ocurrido un error creando la asociacion del Item de menú '%s' y el rol '%s'.", $menuItem->display_name, $role->name);
							$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
						}
					}
				}

				$successMsg = sprintf("Item de menú '%s' creado.", $menuItem->display_name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
			}
			else{
				$failMsg = 'Ha ocurrido un error creando el item de menú.';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($menuItem->errors(), "error");
				$this->Flash->error($failMsg);
			}
			return $this->redirect(['action' => 'indexMenu']);
		}
	}

	public function deleteMenu($id){
		$menuItem = $this->AdmMenu->get($id);
		$loguser = $this->Auth->user(); //logged user
		if( $this->AdmMenu->delete($menuItem) ) {
			$successMsg = sprintf("Item de menú eliminado.");
			$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
			$this->Flash->success($successMsg);
		}
		else {
			$failMsg = 'Ha ocurrido un error eliminando el item de menú.';
			$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
			$this->log($menuItem->errors(), "error");
			$this->Flash->error($failMsg);
		}
		return $this->redirect(['action' => 'indexMenu']);
	}

	private function getMenuItems(){
		$menuItems = $this->AdmMenu->find()
		->select(['AdmMenu.id', 'AdmMenu.menu_id', 'AdmMenu.display_name'])
		->contain(['AdmMenuChild'])
		->where(['AdmMenu.menu_id is null'])
		->order(['AdmMenu.position, AdmMenu.display_name ASC'])->toArray();

		$nodes = [];
		foreach ($menuItems as $menuItem) {
			$node = ['text' => $menuItem['display_name'],
			'href' => $menuItem['id'],
			'nodes' => $this->getInner($menuItem->Childs)
		];
		$nodes[] = $node;
	}
	return $nodes;
}

private function getInner($childs){
	$nodes = [];
	if(!empty($childs)){
		foreach ($childs as $child) {
			$node = ['text' => $child->display_name,
			'href' => $child->id,
			'nodes' => $this->getInner($child->Childs)
		];
		$nodes[] = $node;
	}
}
return $nodes;
}
}

?>
