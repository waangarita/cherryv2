<?php
use Cake\Routing\Router;
$path = Router::url('/', true);
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

 $total_pedido =0;
 foreach ($user AS $u) {
  $first_name = $u->first_name;
  $last_name = $u->last_name;
  $email = $u->email;
  $list_price = $u->tbl_client->id_type_list_price;
  $telefono = $u->phone1;
  $telefono2 = $u->phone2;
  $company = $u->tbl_client->name;
  $code_erp = $u->id_client;
  $id_user = $u->id;
 }

  foreach ($detail as $det):
    $total_pedido = $total_pedido + (number_format($det['price_product'] * $det['amount'], 2));
  endforeach;

  $date = new DateTime($detail[0]['created']);
?>

<body>
  <div>
    <img src="http://cherryext.miro.beecloud.me/images/cherry-logo.png" width="220px" alt="">
  </div>
  <hr>
  <br>
  <div>
    <h1> Detalle Pedido </h1>
    <h3> Fecha pedido: <?= $date->format('m-d-Y H:i') ?> &nbsp;&nbsp; | &nbsp;&nbsp; Pedido#: <?= $code ?> </h3>
  </div>
  <div>
    <table width="100%" style="border: 1px solid #999;border-collapse: collapse;">
      <thead>
        <tr>
          <th style="border: 1px solid #999;text-align: center;padding: 0.5rem;" width="50%">Comprador</th>
          <th style="border: 1px solid #999;text-align: center;padding: 0.5rem;" width="50%">Detalle</th>
        </tr>
      </thead>
      <tbody>
        <tr >
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Codigo ERP:</b> <?= $code_erp ?> </td>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Pedido#: </b> <?= $code ?> </td>
        </tr>
        <tr>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Empresa:</b> <?= $company ?> </td>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Items: </b> <?= count($detail) ?>  </td>
        </tr>
        <tr>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Comprador:</b> <?= $first_name ?> <?= $last_name ?> </td>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Total Pedido:</b> $<?= number_format($total_pedido,2) ?>  </td>
        </tr>
        <tr>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Email:</b> <?= $email ?> </td>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Lista Precios Aplicada: </b> <?= $list_price ?> </td>
        </tr>
        <tr>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Telefono:</b> <?= $telefono ?> </td>
          <td style="padding: 0.5rem;border-right:1px solid #999;"><b>Forma envio: </b> <?= $shipping_way ?> </td>
        </tr>
        <tr>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Telefono 2:</b> <?= $telefono2 ?> </td>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Transportadora: </b> <?= $shipping_name ?> </td>
        </tr>
        <tr>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> </td>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Notas adicionales: </b> <?= $notes ?> </td>
        </tr>
        <tr>
          <td style="padding: 0.5rem;border-right:1px solid #999;"></td>
          <td style="padding: 0.5rem;border-right:1px solid #999;"> <b>Orden compra : </b> <?= $purchase_order ?> </td>
        </tr>
      </tbody>
    </table>
  </div>
  <br>
  <div>
    <table  width="100%" style="border: 1px solid #999;border-collapse: collapse;" >
      <thead>
        <tr>
          <th style="border: 1px solid #999;text-align: center;padding: 0.5rem;" width="50%">Producto</th>
          <th style="border: 1px solid #999;text-align: center;padding: 0.5rem;" width="20%">Precio</th>
          <th style="border: 1px solid #999;text-align: center;padding: 0.5rem;" width="10%">Cantidad</th>
          <th style="border: 1px solid #999;text-align: center;padding: 0.5rem;" colspan="20%">Total</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($detail as $d): ?>
          <tr>
            <td style="border: 1px solid #999;text-align: left;padding: 0.5rem;">
              <b style="font-size:15pt;">Codigo: <?= $d['code'] ?></b>
              <p><b>Tipo producto:</b> <?= $d['type_product'] ?></p>
              <p><b>Series:</b> <?= $d['product_series'] ?> </p>
              <p><b>Modelos:</b> <?= $d['models'] ?> </p>
            </td>
            <td style="border: 1px solid #999;text-align: center;padding: 0.5rem;font-size:15pt;font-weight:bold;">$<?= number_format($d['price_product'],2) ?></td>
            <td style="border: 1px solid #999;text-align: center;padding: 0.5rem;font-size:15pt;font-weight:bold;"><?= $d['amount'] ?></td>
            <td style="border: 1px solid #999;text-align: center;padding: 0.5rem;font-size:15pt;font-weight:bold;">$<?= number_format($d['price_product'] * $d['amount'], 2) ?></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="3"></td>
          <td style="border: 1px solid #999;text-align: center;padding: 0.5rem;font-size:15pt;font-weight:bold;">TOTAL &nbsp;&nbsp; $<?= number_format($total_pedido,2) ?></td>
        </tr>
      </tfoot>
    </table>
  </div>
</body>
