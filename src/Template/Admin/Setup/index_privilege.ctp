<?php
	$this->assign('title', __('Configuracion de roles'));
	$this->assign('section', __('Configuracion de roles'));
?>
<section class="content-header">
	<h1>
        <?= $this->fetch('section') ?>
        <small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
        <li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
        <div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de Roles') ?></h3>
        </div>
        <div class="box-body">
			<?php if( $roles->count() == 0 ): ?>
			<div class="alert alert-info">
				<?= __('No hay roles para configurar.') ?>
			</div>
			<?php else: ?>
			<table class="table table-condensed table-striped table-hover">
				<tr>
					<th><?php echo $this->Paginator->sort('name', __('Rol')); ?></th>
					<th>&nbsp;</th>
				</tr>
				<?php foreach($roles as $role): ?>
				<tr>
					<td>
						<?= h($role->name) ?>
					</td>
					<td class="text-right">
						<a href="<?= $this->Url->build(['action' => 'config_privilege', $role->id]) ?>" class="btn btn-warning btn-sm setup_config_privilege" title="Ver">
							<i class="fa fa-edit"></i>
						</a>
					</td>
				</tr>
				<?php endforeach; ?>
			</table>
			<ul class="pagination">
				<?= $this->Paginator->first('«') ?>
				<?= $this->Paginator->numbers() ?>
				<?= $this->Paginator->last('»') ?>
			</ul>
			<?php endif; ?>
        </div>
	</div>
</section>