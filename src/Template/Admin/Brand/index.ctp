<?php
$this->assign('title', __('Marcas'));
$this->assign('section', __('Marcas'));
?>
<section class="content-header">
	<h1>
		<?= $this->fetch('section') ?>
		<small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de marcas') ?></h3>
			<a href="<?= $this->Url->build(['action' => 'add_brand']) ?>" class="btn btn-default btn-sm brand_add_brand" title="Agregar" style="float: right;">
				<i class="fa fa-plus"></i>
			</a>
		</div>
		<div class="box-body">
			<?php if( $brands->count() == 0 ): ?>
				<div class="alert alert-info">
					<?= __('No hay marcas para mostrar.') ?>
				</div>
			<?php else: ?>
				<table class="table dataTables table-bordered table-striped">
					<thead>
						<tr>
							<th><?= __('Código') ?></th>
							<th><?= __('Nombre') ?></th>
							<th style="text-align: center;"><?= __('Fecha creación') ?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($brands as $brand): ?>
							<tr>
								<td>
									<?= h($brand->code) ?>
								</td>
								<td>
									<?= h($brand->name) ?>
								</td>
								<td align="center">
									<?= h($brand->created->format('m/d/y h:i')) ?>
								</td>
								<td class="text-right">
									<a href="<?= $this->Url->build(['action' => 'edit_brand', $brand->code]) ?>" class="btn btn-warning btn-sm brand_edit_brand">
										<i class="fa fa-edit"></i>
									</a>
									<a href="<?= $this->Url->build(['action' => 'delete_brand', $brand->code]) ?>" class="btn btn-danger btn-sm brand_delete_brand"
										onclick="return confirm('<?= __("Si usted elimina esta marca se eliminaran todas las lineas y productos que estan asociados a ella, ¿Esta seguro que desea eliminar $brand->name ")  ?>?');">
										<i class="fa fa-trash-o"></i>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	</div>
</section>
