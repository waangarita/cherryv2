<?php
$this->assign('title', __('Lineas'));
$this->assign('section', __('Lineas'));
?>
<section class="content-header">
	<h1>
		<?= $this->fetch('section') ?>
		<small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de lineas') ?></h3>
			<a href="<?= $this->Url->build(['action' => 'add_family']) ?>" class="btn btn-default btn-sm family_add_family" title="Agregar" style="float: right;">
				<i class="fa fa-plus"></i>
			</a>
		</div>
		<div class="box-body">
			<?php if( $families->count() == 0 ): ?>
				<div class="alert alert-info">
					<?= __('No hay lineas para mostrar.') ?>
				</div>
			<?php else: ?>
				<table class="table dataTables table-bordered table-condensed table-striped table-hover">
					<thead>
						<tr>
							<th><?= __('Còdigo') ?></th>
							<th><?= __('Nombre') ?></th>
							<th><?= __('Marca') ?></th>
							<th style="text-align: center;"><?= __('Fecha creacion') ?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($families as $family): ?>
							<tr>
								<td>
									<?= h($family->code) ?>
								</td>
								<td>
									<?= h($family->name) ?>
								</td>
								<td>
									<?= h($family->tbl_brand->name) ?>
								</td>
								<td align="center">
									<?= h($family->created->format('m/d/y h:i')) ?>
								</td>
								<td class="text-right">
									<a href="<?= $this->Url->build(['action' => 'edit_family', $family->code]) ?>" class="btn btn-warning btn-sm family_edit_family">
										<i class="fa fa-edit"></i>
									</a>
									<a href="<?= $this->Url->build(['action' => 'delete_family', $family->code]) ?>" class="btn btn-danger btn-sm family_delete_family"
										onclick="return confirm('<?= __("Si usted elimina esta linea se eliminaran todos productos que estan asociados a ella, ¿Esta seguro que desea eliminar $family->name ")  ?>?');">
										<i class="fa fa-trash-o"></i>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	</div>
</section>
