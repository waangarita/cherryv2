<?php

namespace App\Controller\Admin;

class OrderController extends AdminController {

	public function initialize()
	{
		parent::initialize();

		// load models
		$this->loadModel('TblOrder');
		$this->loadModel('TblDetailOrder');
	}

	/*
	* Orders management
	*/

	public function index() {
		$orders = $this->TblOrder->find()->order(['TblOrder.created ASC']);
		$detail_order = $this->TblOrder;
		debug($detail_order);
		$this->set(compact('orders'));
		$this->set(compact('detail_order'));
	}

	public function viewOrder ($id) {
		$order = $this->TblOrder->get($id);
		$detail_order = $this->TblOrder->detailOrder($order->id_user,$id);
		$this->set(compact('order'));
		$this->set(compact('detail_order'));

		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			$order->status = $this->request->data('status');
			if( $this->TblOrder->save($order) ) {
				$successMsg = sprintf("Pedido #%s editado.", $order->id);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
			} else {
				$failMsg = 'Ha ocurrido un error actualizando la orden '.$order->id;
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($client->errors(), "error");
				$this->Flash->error($failMsg);
			}
			return $this->redirect(['action' => 'index']);
		}
	}

}

?>
