<?php

namespace App\Controller\Admin;

class ClientController extends AdminController {

	public function initialize()
	{
		parent::initialize();

		// load models
		$this->loadModel('TblClient');
		$this->loadModel('TblListPrice');
		$this->loadModel('AdmUser');
	}

	/*
	* Client management
	*/

	public function index() {
		$clients = $this->TblClient->find()
									->contain(['TblListPrice' => function ($q) {
										return $q->select(['name']);
									}])
									->where(['active' => 1])
									->order(['TblClient.code']);
		$this->set(compact('clients'));
	}

	public function addClient() {
		$tpListPrice = $this->TblListPrice->find()->select(['name']);
		if($this->request->is('post'))
		{
			$loguser = $this->Auth->user(); //logged user
			$client = $this->TblClient->newEntity($this->request->data());
			$client->code = $this->request->data('code');

			if($this->TblClient->save($client))
			{
				$successMsg = sprintf("Empresa '%s' creado.", $client->name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success( sprintf("['%s] ha sido creada correctamente.", $client->name));
				return $this->redirect(['action' => 'index']);
			}
			else
			{
				$successMsg = sprintf("Ha ocurrido un error creando '%s' ", $client->name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$client->errors();
				$this->Flash->error(sprintf("[%s] no pudo ser creado, intente nuevamente.", $client->name));
			}
		}
		$this->set(compact('tpListPrice'));
		$this->set(compact('client'));
	}

	public function editClient ($id) {
		$client = $this->TblClient->get($id);
		$tpListPrice = $this->TblListPrice->find();
		$this->set(compact('client'));
		$this->set(compact('tpListPrice'));

		if( $this->request->is('post') ) {
			$loguser = $this->Auth->user(); //logged user

			$client = $this->TblClient->patchEntity($client, $this->request->data());
			if( $this->TblClient->save($client) ) {
				$successMsg = sprintf("'%s' editada.", $client->name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
			} else {
				$failMsg = sprintf("Ha ocurrido un error editando ['%s'] ", $client->name);
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->log($client->errors(), "error");
				$this->Flash->error($failMsg);
			}
			return $this->redirect(['action' => 'index']);
		}
	}

	public function deleteClient ($id) {
		$client = $this->TblClient->get($id);
		$clientName = $client->name;
		$client->active = 0;
		$loguser = $this->Auth->user(); //logged user
		try{
			if( $this->TblClient->save($client) ) {
				$this->AdmUser->updateAll(array('status' => 'DELETED'), array('id_client' => $id));
				$successMsg = sprintf("cliente [%s] eliminado.", $clientName);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success($successMsg);
				return $this->redirect(['action' => 'index']);
			}
		} catch (\Exception $e) {
			$failMsg = 'Error al eliminar el cliente';
			$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
			$this->Flash->error($failMsg);
			return $this->redirect(['action' => 'index']);
		}
	}
}

?>
