<?php
$this->assign('title', __('Menu'));
$this->assign('section', __('Menu'));
?>
<section class="content-header">
    <h1>
        <?= $this->fetch('section') ?>
        <small><?= __('Crear') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?= $this->Url->build(['action' => 'indexMenu']) ?>">
                <?= $this->fetch('section') ?>
            </a>
        </li>
        <li class="active"><?= __('Crear') ?></li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-body">
            <form method="post" enctype="multipart/form-data">
                <div class="form-group has-feedback">
                    <label><?= __('Titulo para mostrar') ?></label>
                    <input type="text" name="display_name" class="form-control" placeholder="<?= __('Titulo para mostrar') ?>" required>
                    <span class="form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label><?= __('Icono') ?></label>
                    <input type="text" name="icon" class="form-control" placeholder="<?= __('Icono') ?>">
                    <a href="https://almsaeedstudio.com/themes/AdminLTE/pages/UI/icons.html" target="_blank"><?= __('Referencias de iconos') ?></a>
                    <span class="form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <label><?= __('Posicion') ?></label>
                    <input type="text" name="position" class="form-control" placeholder="<?= __('Posicion') ?>" value="1" required>
                    <span class="form-control-feedback"></span>
                </div>
            	<?php if(!empty($menuItems)){ ?>
            	<div class="form-group has-feedback">
            		<input type="hidden" id="menu_id" name="menu_id" value=""/>
					<label><?= __('Seleccione padre') ?></label>
                    <div id="tree"></div>
            	</div>
            	<?php } ?>
                <div class="form-group has-feedback">
                    <label><?= __('Seleccionar seccion asociada') ?></label>
                    <select name="section_id" class="form-control">
                        <?php foreach ($sections as $section) { ?>
                            <option value="<?= $section->id?>"><?= $section->name ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="role_id"><?= __('Visible para los roles') ?></label>
                    <select id="role_id" name="role_id[]" class="form-control select2" multiple>
                        <?php foreach($roles as $role): ?>
                        <option value="<?= $role->id ?>"><?= $role->name ?></option>
                        <?php endforeach; ?>
                    </select> 
                </div> 
                <hr>
                <div class="row">
                    <div class="col-xs-4">
                        <a href="<?= $this->Url->build(['action' => 'indexMenu']) ?>" class="btn btn-danger">
                            <?= __('Cancelar') ?>
                        </a>
                    </div>
                    <div class="col-xs-offset-4 col-xs-4 text-right">
                        <button type="submit" class="btn btn-primary setup_add_menu"><?= __('Guardar') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<?php $this->append('scripts'); ?>
<script type="text/javascript">
// Tree view
$('#tree').treeview({data: <?= json_encode($menuItems) ?> });
$('#tree').treeview('expandAll', { silent: true });

$('#tree').on('nodeSelected', function(event, data) {
  // Your logic goes here
  $('#menu_id').val(data.href);
});

// Select 2 code
$('.select2').select2({ placeholder: "<?= __('Seleccione') ?>", width: "100%" });
</script>
<?php $this->end(); ?>
