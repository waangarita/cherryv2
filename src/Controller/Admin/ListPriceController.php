<?php

namespace App\Controller\Admin;

class ListPriceController extends AdminController {

	public function initialize()
	{
		parent::initialize();

		// load models
		$this->loadModel('TblListPrice');
		$this->loadModel('TblClient');
		$this->loadModel('TblDetailListPrice');
		$this->loadModel('TblFileUpload');
		$this->loadModel('TblReport');

	}

	/*
	* Family management
	*/

	public function index() {
		if(isset($this->request->query['hash'])) {
			$count = $this->TblListPrice->countReport();
			$this->set(compact('count'));

			$report = $this->TblFileUpload->getReport($this->request->query['hash']);
			$this->set(compact('report'));
		}

		$list_price = $this->TblListPrice->find()
										->contain(['TblDetailListPrice' => function ($q) {
												return $q->select(['id_list_price']);
											}
										]);

		$this->set(compact('list_price'));
	}

	public function addListPrice () {
		$list_price = $this->TblListPrice->find();
		if($this->request->is('post'))
		{
			$loguser = $this->Auth->user(); //logged user
			$list_price = $this->TblListPrice->newEntity($this->request->data());
			$list_price->name = $this->request->data('name');
			$list_price->active = 1;
			if($this->TblListPrice->save($list_price))
			{
				$successMsg = sprintf("Lista de precios '%s' creada.", $list_price->name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$this->Flash->success('La lista de precios ha sido creada correctamente.');
				return $this->redirect(['action' => 'index']);
			}
			else
			{
				$successMsg = sprintf("Ha ocurrido un error creando la '%s' .", $list_price->name);
				$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
				$family->errors();
				$this->Flash->error('La lista de precios no pudo ser creada, intente nuevamente.');
			}
		}

		$this->set(compact('list_price'));
	}

	public function viewListPrice ($id) {
		$list_price = $this->TblListPrice->get($id);
		$detail_list = $this->TblDetailListPrice->find()
												->contain(['TblProduct' => function ($q) {
														return $q->select(['code','type_product']);
													}
												])
												->where(['id_list_price = ' => $id ]);
		$this->set('detail_list', $detail_list);
		$this->set('list_price', $list_price);
	}

	public function deleteListPrice ($id) {
		$client = $this->TblClient->searchClient($id);

		if($client) {
			$this->Flash->warning(__('Hay clientes asociados a esta lista de precios , por favor desvincularlos para poder eliminar la lista de precios '));
		} else {
			$list_price = $this->TblListPrice->get($id);
			$loguser = $this->Auth->user(); //logged user
			try {
				if( $this->TblListPrice->delete($list_price) ) {
					$successMsg = sprintf("Lista precio eliminada. [%s]", $list_price->name);
					$this->log(sprintf("%s by '%s'", $successMsg, $loguser['email']), 'info');
					$this->Flash->success($successMsg);
				}
			} catch (\Exception $e) {
				$failMsg = 'Error al eliminar la lista precio';
				$this->log(sprintf("%s by '%s'", $failMsg, $loguser['email']), 'error');
				$this->Flash->error($failMsg);
			}
		}

		return $this->redirect(['action' => 'index']);
	}

	public function uploadList () {
		if($this->request->is('post')) {
			$loguser = $this->Auth->user(); //logged user
			$file = $_FILES['file_list'];
			// Check if directory exists make sure it has correct permissions, if not make it
			if (is_dir($destination_directory = WWW_ROOT . 'files' . DS . 'list_price')) {
				chmod($destination_directory, 0775);
			} else {
				mkdir($destination_directory, 0775, true);
			}

			$filename = $file['name'];
			$csvFile = $destination_directory . DS . $file['name'];

			//upload csv file
			if (move_uploaded_file($file['tmp_name'], $csvFile)) {
				$this->TblReport->deleteAll(['hashFile <>' => 0]);
				$hoy = date('y-m-d h:i:s');
				$hash = md5($hoy.$csvFile);

				if ($this->TblListPrice->loadDataLocalInfile($csvFile, $hash)) {
					$this->Flash->success('Listas de precios cargadas éxito!');
				} else {
					$this->Flash->error('Ha ocurrido un error al cargar las listas de precios, por favor verifique que se encuentran creadas todas las listas de precios');
				}
			}
		}

		return $this->redirect(['action' => 'index', 'hash' => $hash ]);
	}
}

?>
