<?php
$this->assign('title', __('Lineas'));
$this->assign('section', __('Lineas'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Editar') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Editar') ?></li>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-body">
      <form method="post" enctype="multipart/form-data" enctype="multipart/form-data">
        <div class="form-group has-feedback">
          <label><?= __('Marca')  ?> : <?= $family->tbl_brand['name'] ?> </label>
          <input type="hidden" name="id_brand" value="<?= h($family->id_brand) ?>">
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Código') ?> <span class="text-danger">*</span></label>
          <input type="text" readonly name="code" class="form-control" placeholder="<?= __('Còdigo') ?>" value="<?= $family->code ?>" >
          <span class="form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Nombre') ?> <span class="text-danger">*</span></label>
          <input type="text" name="name" class="form-control" placeholder="<?= __('Nombre') ?>" value="<?= $family->name ?>" required="true">
          <span class="form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Descripcion') ?></label>
          <textarea name="description" class="form-control" rows="4"><?= h($family->description) ?></textarea>
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Imagen relacionada') ?></label>
          <input type="file" accept="image/*" name="file_img" id="img">
        </div>
        <hr>
        <br>
        <?php if ($family->img !== ''): ?>
        <div class="container-fluid">
          <img src="<?= $this->Url->build($family->img) ?>" style="border-radius:10px;border:2px solid #424242;"  width="100%" alt="">
          <br>
          <br>
          <input type="hidden" value="<?= $family->img ?>" name="delete_img">
          <button type="submit" onclick="return confirm('<?= __("Esta seguro que desea eliminar esta imagen?") ?>');" class="btn btn-danger btn-block btn-img"> 
            <i class="fa fa-trash"></i> Eliminar imagen 
          </button>
        </div>
        <hr>
        <?php endif; ?>
        <div class="row">
          <div class="col-xs-4">
            <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-danger">
              <?= __('Cancelar') ?>
            </a>
          </div>
          <div class="col-xs-offset-4 col-xs-4 text-right">
            <button type="submit" class="btn btn-primary family_edit_family"><?= __('Guardar') ?></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
