<?php
$this->assign('title', __('Seccion'));
$this->assign('section', __('Seccion'));
?>
<section class="content-header">
    <h1>
        <?= $this->fetch('section') ?>
        <small><?= __('Crear') ?></small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?= $this->Url->build(['action' => 'indexSection']) ?>">
                <?= $this->fetch('section') ?>
            </a>
        </li>
        <li class="active"><?= __('Crear') ?></li>
    </ol>
</section>

<section class="content">

    <div class="box">
        <div class="box-body">
            <form method="post" enctype="multipart/form-data">
                <div class="form-group has-feedback">
                    <label><?= __('Titulo para mostrar') ?></label>
                    <input type="text" name="name" class="form-control" placeholder="<?= __('Titulo para mostrar') ?>" required>
                    <span class="form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
					<label><?= __('Controlador') ?></label>
                    <input type="text" name="controller" class="form-control" value="" placeholder="<?= __('Controlador') ?>">
                    <span class="form-control-feedback"></span>
            	</div>
            	<div class="form-group has-feedback">
					<label><?= __('Metodo') ?></label>
                    <input type="text" name="action" class="form-control" value="" placeholder="<?= __('Metodo') ?>">
                    <span class="form-control-feedback"></span>
            	</div>
                <hr>
                <div class="row">
                    <div class="col-xs-4">
                        <a href="<?= $this->Url->build(['action' => 'indexSection']) ?>" class="btn btn-danger">
                            <?= __('Cancelar') ?>
                        </a>
                    </div>
                    <div class="col-xs-offset-4 col-xs-4 text-right">
                        <button type="submit" class="btn btn-primary setup_add_section"><?= __('Guardar') ?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

