<!DOCTYPE html>
<html lang="en">
<head>
  <title> cherry </title>
  <meta charset="utf-8">
  <meta name="description" content="cherry Products">
  <meta name="author" content="" />
  <meta name="keywords" content="global, cherry, products" />
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">

  <meta http-equiv="X-UA-Compatible" content="IE=edge" />

  <!--Favicon-->
  <link rel="shortcut icon" href="<?= $this->Url->build('/img/favicon.ico') ?>" type="image/x-icon">
  <link rel="icon" href="<?= $this->Url->build('/img/favicon.ico') ?>" type="image/x-icon">

  <!-- css files -->
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/bootstrap.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/font-awesome.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/owl.carousel.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/owl.theme.default.min.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/animate.css') ?>" />
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/swiper.css') ?>" />

  <!-- this is default skin you can replace that with: dark.css, yellow.css, red.css ect -->
  <link id="pagestyle" rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/skin-blue.css') ?>" />

  <!-- Nuevos Estilos cherry -->
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/maqueta/css/new_style.css') ?>" />

  <!-- Google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&amp;subset=latin-ext" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?= $this->Url->build('/css/sweetalert.css') ?>">

</head>
<body>
  <!-- start topBar -->
  <div class="topBar inverse">
    <div class="container">
      <ul class="list-inline pull-left hidden-sm hidden-xs">
        <li><span class="text-primary"> <?= __('Tienes alguna pregunta') ?>? </span> <?= __('Llamanos') ?> +1 305 477 2988 </li>
      </ul>

      <ul class="topBarNav pull-right">
        <?php if ($language == 'ES'): ?>
        <li class="linkdown">
          <a href="javascript:void(0);">
            <img src="<?= $this->Url->build('/maqueta/img/flags/flag-spain.jpg') ?>" class="mr-5" alt="">
            <span class="hidden-xs">
              Español
              <i class="fa fa-angle-down ml-5"></i>
            </span>
          </a>
          <ul class="w-100">
            <li><a href="" onclick="language('EN',event)"><img src="<?= $this->Url->build('/maqueta/img/flags/flag-english.jpg') ?>" class="mr-5" alt=""> English </a></li>
          </ul>
        </li>
        <?php else: ?>
          <li class="linkdown">
            <a href="javascript:void(0);">
              <img src="<?= $this->Url->build('/maqueta/img/flags/flag-english.jpg') ?>" class="mr-5" alt="">
              <span class="hidden-xs">
                English
                <i class="fa fa-angle-down ml-5"></i>
              </span>
            </a>
            <ul class="w-100">
              <li>
                <a href="" onclick="language('ES',event)">
                  <img src="<?= $this->Url->build('/maqueta/img/flags/flag-spain.jpg') ?>" class="mr-5" alt=""> Español
                </a>
              </li>
            </ul>
          </li>
        <?php endif; ?>
        <li class="linkdown">
          <a href="javascript:void(0);">
            <i class="fa fa-user mr-5"></i>
            <span class="hidden-xs">
              <?= __('Mi cuenta') ?>
              <i class="fa fa-angle-down ml-5"></i>
            </span>
          </a>
          <ul class="w-150">
            <li><a href="<?= $this->Url->build(['action' => 'my-account']) ?>"><?= __('Mi cuenta') ?></a></li>
            <li class="divider"></li>
            <li><a href="<?= $this->Url->build(['action' => 'logout']) ?>"><?= __('Cerrar Sesion') ?></a></li>
          </ul>
        </li>
        <?= $this->element('Partials/cart') ?>
        <li class="linkdown">
          <a href="<?= $this->Url->build(['action' => 'logout']) ?>"> <i class="fa fa-sign-out"></i> <?= __('Cerrar Sesion') ?></a>
        </li>
      </ul>
    </div><!-- end container -->
  </div>
  <!-- end topBar -->

  <div class="middleBar" >
    <div class="container">
      <div class="row display-table" style="margin-bottom:15px;">
        <div  class="col-sm-3 vertical-align  text-left hidden-xs">
          <a href="<?= $this->Url->build(['action' => 'index']) ?>">
            <img width="220" src="<?= $this->Url->build('/images/cherry-logo.png') ?>" alt="" />
          </a>
        </div><!-- end col -->
        <div class="col-sm-7 vertical-align text-center">
          <form action="<?= $this->Url->build(['action' => 'products']) ?>">
            <div class="row grid-space-1">
              <div class="col-sm-9 col-xs-9">
                <input  type="text" name="search" class="form-control input-lg radius_default" placeholder="<?= __('Buscar') ?> ">
              </div><!-- end col -->

              <div class="col-sm-2 col-xs-2">
                <button  type="submit" id="btn-search" class="btn btn-default btn-lg radius_default"><i class="fa fa-search"></i></button>
              </div><!-- end col -->
            </div><!-- end row -->
          </form>
        </div><!-- end col -->

      </div><!-- end  row -->
    </div><!-- end container -->
  </div><!-- end middleBar -->

  <!-- start navbar -->
  <div class="navbar yamm navbar-default">
    <div class="container">
      <div class="navbar-header">
        <button type="button" data-toggle="collapse" data-target="#navbar-collapse-3" class="navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="navbar-brand visible-xs">
          <img src="<?= $this->Url->build('/images/cherry-logo.png') ?>" width="200px"  alt="logo">
        </a>
      </div>
      <div class="col-md-4"></div>
      <div  id="navbar-collapse-3" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li><a href="<?= $this->Url->build(['action' => 'index']) ?>"><?= __('Inicio') ?></a></li>

          <li><a href="<?= $this->Url->build(['action' => 'products']) ?>"><?= __('Productos') ?></a></li>

          <li><a href="<?= $this->Url->build(['action' => 'promotion']) ?>"><?= __('Promociones') ?> </a></li>

          <li><a href="<?= $this->Url->build(['action' => 'faq']) ?>"><?= __('FAQ') ?> </a></li>
        </ul>
      </div><!-- end navbar collapse -->
    </div><!-- end container -->
  </div><!-- end navbar -->

  <!-- start section -->
  <section class="section light-backgorund">
    <div class="container">
      <div class="row">
        <div class="col-sm-4 col-md-3">
          <!--  NAVBAR-VERTICAL BRANDS WITH FAMILIES -->
          <?= $this->element('Partials/navbar-vertical') ?>
          <!-- END NAVBAR-VERTICAL BRANDS WITH FAMILIES -->

          <hr class="spacer-20 no-border">

          <div class="widget hidden-xs">
            <h6 class="subtitle text-uppercase"><?= __('Precios Mejorados') ?></h6>

            <ul class="items">
              <?php foreach ($prices_best AS $product): ?>
                <?php if ($product['price_product'] > 0 ): ?>
                  <li>
                    <a href="<?= $this->Url->build(['action' => 'detail_product', $product['code'] ]) ?>" class="product-image">
                      <img src="<?= $this->Url->build($product['img']) ?>" alt="<?= $product['code'] ?>">
                    </a>
                    <div class="product-details">
                      <p class="product-name">
                        <a href="<?= $this->Url->build(['action' => 'detail_product', $product['code'] ]) ?>">
                          <b><?= $product['type_product'] ?></b>
                          <br>
                          <?= $product['product_series'] ?>
                        </a>
                      </p>
                      <?php if ($auth['role_id'] <> 3 ) : ?>
                        <span class="price text-primary">$ <?= number_format($product['price_product'],2) ?></span>
                      <?php endif; ?>
                    </div>
                  </li>
                <?php  endif; ?>
              <?php endforeach; ?>
            </ul>
          </div><!-- end widget -->
        </div><!-- end col -->

        <div class="col-sm-8 col-md-9">
          <!-- SLIDE CAROUSEL -->
          <div class="row">
            <div class="col-sm-12">
              <div class="owl-carousel slider owl-theme">
                <?php foreach ($components['sliders'] AS $slide) : ?>
                  <div class="item">
                    <figure>
                      <a href='<?= $this->Url->build($slide->cta) ?>' <?= ($slide->isTargetBlank() ? " target=\"_blank\"" : "") ?> >
                        <img src="<?= $this->Url->build($slide->img_desktop) ?>" onerror="this.onerror=null;this.src=`<?= $this->Url->build('images/products/no-image.png') ?>`" width="100%" alt=""/>
                      </a>
                    </figure>
                  </div><!-- end item -->
                <?php endforeach;?>
              </div><!-- end owl carousel -->
            </div><!-- end col -->
          </div><!-- end row -->
          <!-- END SLIDE CAROUSEL -->

          <hr class="spacer-20 no-border">

          <!--  NEW PRODUCTS -->
          <div class="row">
            <div class="col-sm-12">
              <h6 class="ml-5 mb-20 text-uppercase"><span class="text-primary"> <?= __('Nuevos') ?></span> <?= __('Productos') ?></h6>
            </div><!-- end col -->
            <div class="col-sm-12">
              <div class="owl-carousel column-3 owl-theme">
                <?php foreach ($new_products AS $product): ?>
                  <?php if ($product['price_product'] > 0 ): ?>
                    <!-- ONE PRODUCT -->
                    <div class="item">
                      <div class="thumbnail store">
                        <div class="header">
                          <div class="badges">
                            <span class="product-badge top left primary-background text-white semi-circle"><?= __('Nuevo') ?></span>
                          </div>
                          <a href="<?= $this->Url->build(['action' => 'detail_product', $product['code']]) ?>">
                            <figure class="layer">
                              <img src="<?= $this->Url->build($product['img']) ?>" alt="<?= $product['code'] ?>" >
                            </figure>
                          </a>
                        </div>
                        <div class="caption">
                          <h6 class="regular">
                            <a href="<?= $this->Url->build(['action' => 'detail_product', $product['code']]) ?>">
                              <b><?= $product['code'] ?></b>
                              <br><?= $product['type_product'] ?>
                              <br><b><?= strtoupper(substr($product['product_series'],0,22)) ?></b>
                              <br><?= strtoupper(substr($product['models'],0,22)) .'...' ?>
                            </a>
                          </h6>
                          <?php if ($auth['role_id'] <> 3 ) : ?>
                            <div class="price">
                              <div class="col-md-6 col-xs-6">
                                <span class="amount text-primary">$ <?= number_format($product['price_product'], 2) ?></span>
                              </div>
                              <?php if($product['iscart'] == 0):  ?>
                                <div class="col-md-6 col-xs-6 cant_product">
                                  <span class="col-md-6 col-xs-6">Cant.</span>
                                  <div class="col-md-6 col-xs-6">
                                    <input type="number" min="1" title="Requerido" class="form-control" name="<?= $product['code'] ?>" id="<?= $product['code'] ?>" onKeyPress="javascript:return solo_numeros ( event )" />
                                  </div>
                                </div>
                              <?php endif; ?>
                            </div>
                            <div id="btn<?= $product['code'] ?>" class="col-md-12 col-xs-12 add_cart_botton">
                              <?php if($product['iscart'] == 0):  ?>
                                <a href="" onclick="add_cart('<?= $product['code'] ?>',<?= $auth['id'] ?>, event)"><i class="fa fa-cart-plus mr-5"></i><?= __('Agregar a Pedido') ?></a>
                              <?php else : ?>
                                <a class="text-success"><i class="fa fa-check-circle mr-5"></i> <?= __('add_to_cart') ?> </a>
                              <?php endif; ?>
                            </div>
                          <?php endif; ?>
                        </div>
                      </div><!-- end thumbnail -->
                    </div><!-- end item -->
                    <!-- END ONE PRODUCT -->
                  <?php endif; ?>
                <?php endforeach; ?>
              </div><!-- end owl carousel -->

            </div><!-- end col -->
          </div><!-- end row -->
          <!-- END NEW PRODUCTS -->
        </div><!-- end col -->
      </div><!-- end row -->
    </div><!-- end container -->
  </section>
  <!-- end section -->

  <?= $this->fetch('content') ?>

  <!-- BANNER INFERIOR Y FOOTER -->
  <?= $this->element('Partials/footer') ?>
  <!-- END BANNER INFERIOR Y FOOTER -->

  <!-- LENGUAGE -->
  <script type="text/javascript">
    const LANGUAGE = '<?= $language  ?>'
    const API = '<?= $this->Url->build("/api/") ?>'
    const URL_IMAGES = '<?= $this->Url->build('/images/') ?>'
    const ROOT = '<?= $this->Url->build('/') ?>'
  </script>
  <!-- END LENGUAGE -->

  <!-- JavaScript Files -->
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/jquery-3.1.1.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/bootstrap.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/owl.carousel.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/jquery.downCount.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/nouislider.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/jquery.sticky.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/pace.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/star-rating.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/wow.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/gmaps.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/swiper.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/maqueta/js/main.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/js/translate.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/js/sweetalert.min.js') ?>"></script>
  <script type="text/javascript" src="<?= $this->Url->build('/js/general.js') ?>"></script>
  <script>
    $(document).ready(function () {
      getCartByUser (<?= $auth['id'] ?>)
    })
  </script>
  <?= $this->fetch('scripts') ?>
</body>
</html>
