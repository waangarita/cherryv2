<?php
$this->assign('title', __('Usuarios'));
$this->assign('section', __('Usuarios'));
?>
<section class="content-header">
	<h1>
		<?= $this->fetch('section') ?>
		<small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de usuarios') ?></h3>
			<a href="<?= $this->Url->build(['action' => 'add_user']) ?>" class="btn btn-default btn-sm setup_add_user" title="Agregar" style="float: right;">
				<i class="fa fa-plus"></i>
			</a>
		</div>
		<div class="box-body">
			<?php if( $users->count() == 0 ): ?>
				<div class="alert alert-info">
					<?= __('No hay usuarios para mostrar.') ?>
				</div>
			<?php else: ?>
				<table class="table table-condensed table-bordered dataTables table-striped table-hover">
					<thead>
						<tr>
							<th><?= __('Nombres') ?></th>
							<th><?= __('Apellidos') ?></th>
							<th> Email </th>
							<th><?= __('Cliente') ?></th>
							<th><?= __('Rol') ?></th>
							<th style="text-align: center;"><?= __('Estado') ?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($users as $user): ?>
							<tr>
								<td>
									<?= h($user->first_name) ?>
								</td>
								<td>
									<?= h($user->last_name) ?>
								</td>
								<td>
									<?= h($user->email) ?>
								</td>
								<td>
									<?= h($user->tbl_client->name) ?>
								</td>
								<td>
									<?= h($user->adm_role->name) ?>
								</td>
								<td align="center">
									<?= h($user->status) ?>
								</td>
								<td class="text-right">
									<a href="<?= $this->Url->build(['action' => 'edit_user', $user->id]) ?>" class="btn btn-warning btn-sm setup_edit_user">
										<i class="fa fa-edit"></i>
									</a>
									<a href="<?= $this->Url->build(['action' => 'delete_user', $user->id]) ?>" class="btn btn-danger btn-sm setup_delete_user"
										onclick="return confirm('<?= __("¿Esta seguro que desea eliminar  a $user->first_name $user->last_name ")  ?>?');" >
										<i class="fa fa-trash-o"></i>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	</div>
</section>
