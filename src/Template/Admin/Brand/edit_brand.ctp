<?php
$this->assign('title', __('Marcas'));
$this->assign('section', __('Marcas'));
?>
<section class="content-header">
  <h1>
    <?= $this->fetch('section') ?>
    <small><?= __('Editar') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
      <a href="<?= $this->Url->build(['action' => 'index']) ?>">
        <?= $this->fetch('section') ?>
      </a>
    </li>
    <li class="active"><?= __('Editar') ?></li>
  </ol>
</section>

<section class="content">
  <div class="box">
    <div class="box-body">
      <form method="post" enctype="multipart/form-data">
        <div class="form-group has-feedback">
          <label><?= __('Código') ?> <span class="text-danger">*</span></label>
          <input type="text" readonly="true" name="code" class="form-control" placeholder="<?= __('Código') ?>" value="<?= $brand->code ?>" >
          <span class="form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <label><?= __('Nombre') ?> <span class="text-danger">*</span></label>
          <input type="text" required="true" name="name" class="form-control" placeholder="<?= __('Nombre') ?>" value="<?= $brand->name ?>" >
          <span class="form-control-feedback"></span>
        </div>
        <hr>
        <div class="row">
          <div class="col-xs-4">
            <a href="<?= $this->Url->build(['action' => 'index']) ?>" class="btn btn-danger">
              <?= __('Cancelar') ?>
            </a>
          </div>
          <div class="col-xs-offset-4 col-xs-4 text-right">
            <button type="submit" class="btn btn-primary brand_edit_brand"><?= __('Guardar') ?></button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
