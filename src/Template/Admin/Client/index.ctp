<?php
$this->assign('title', __('Empresas'));
$this->assign('section', __('Empresas'));
?>
<section class="content-header">
	<h1>
		<?= $this->fetch('section') ?>
		<small><?= __('Listado') ?></small>
	</h1>
	<ol class="breadcrumb">
		<li class="active"><?= $this->fetch('section') ?></li>
	</ol>
</section>
<section class="content">
	<div class="box">
		<div class="box-header with-border">
			<h3 class="box-title"><?= __('Listado de empresas') ?></h3>
			<a href="<?= $this->Url->build(['action' => 'add_client']) ?>" class="btn btn-default btn-sm client_add_client" title="Agregar" style="float: right;">
				<i class="fa fa-plus"></i>
			</a>
		</div>
		<div class="box-body">
			<?php if( $clients->count() == 0 ): ?>
				<div class="alert alert-info">
					<?= __('No hay empresas para mostrar.') ?>
				</div>
			<?php else: ?>
				<table class="table dataTables table-condensed table-striped table-hover">
					<thead>
						<tr>
							<th><?= __('Código ERP') ?></th>
							<th><?= __('Nombre') ?></th>
							<th><?= __('Lista precios') ?></th>
							<th style="text-align: center;"><?= __('Fecha creacion') ?></th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($clients as $client): ?>
							<tr>
								<td>
									<?= h($client->code) ?>
								</td>
								<td>
									<?= h($client->name) ?>
								</td>
								<td>
									<?= h($client->tbl_list_price->name) ?>
								</td>
								<td align="center">
									<?= h($client->created->format('m/d/y h:i')) ?>
								</td>
								<td class="text-right">
									<a href="<?= $this->Url->build(['action' => 'edit_client', $client->code]) ?>" class="btn btn-warning btn-sm client_edit_client">
										<i class="fa fa-edit"></i>
									</a>
									<?php if ($client->code != 1): ?>
										<a href="<?= $this->Url->build(['action' => 'delete_client', $client->code]) ?>" class="btn btn-danger btn-sm client_delete_client"
											onclick="return confirm('<?= __("Si elimina este cliente se eliminaran todos los usuarios que estan asociados a el ¿Esta seguro que desea eliminar $client->name ")  ?>?');">
											<i class="fa fa-trash-o"></i>
										</a>
									<?php endif; ?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
		</div>
	</div>
</section>
