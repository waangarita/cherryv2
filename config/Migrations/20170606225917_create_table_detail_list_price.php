<?php

use Phinx\Migration\AbstractMigration;

class CreateTableDetailListPrice extends AbstractMigration
{
  /**
  * Change Method.
  *
  * Write your reversible migrations using this method.
  *
  * More information on writing migrations is available here:
  * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
  *
  * The following commands can be used in this method and Phinx will
  * automatically reverse them when rolling back:
  *
  *    createTable
  *    renameTable
  *    addColumn
  *    renameColumn
  *    addIndex
  *    addForeignKey
  *
  * Remember to call "create()" or "update()" and NOT "save()" when working
  * with the Table class.
  */
  public function change()
  {
    $table = $this->table('tbl_detail_list_price', ['id' => false]);
    $table  ->  addColumn('id_product', 'string')
            ->  addColumn('id_list_price', 'integer')
            ->  addColumn('price', 'decimal')
            ->  addColumn('created', 'timestamp', array('default' => 'CURRENT_TIMESTAMP', 'null' => 'true' ))
            ->  create();

    $refTable = $this->table('tbl_detail_list_price');
    $refTable ->  addForeignKey('id_product', 'tbl_product', 'code', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
              ->  addForeignKey('id_list_price', 'tbl_list_price', 'id', array('delete' => 'CASCADE', 'update' => 'CASCADE'))
              ->  update();
  }
}
